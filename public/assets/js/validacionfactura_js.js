$(document).ready(function() {
    // Añadir método de validación personalizado para IVA
    $.validator.addMethod("validateIVA", function(value, element) {
        // Obtener el importe bruto
        var importeBruto = parseFloat($('#formFactura input[name="Importe_Bruto"]').val());
        // Calcular el IVA esperado (21% del importe bruto)
        var ivaEsperado = importeBruto * 0.21;
        // Comparar el IVA ingresado con el esperado (permitiendo una pequeña tolerancia para errores de redondeo)
        return this.optional(element) || Math.abs(value - ivaEsperado) < 0.01;
    }, "El importe del IVA debe ser el 21% del importe bruto.");

    $('#formFactura').validate({
        rules: {
            Importe_Bruto: {
                required: true,
                number: true,
                min: 30
            },
            Importe_IVA: {
                required: true,
                number: true,
                validateIVA: true // Aplicar la validación personalizada
            },
            Importe_IRPF: {
                required: true,
                number: true,
                min: 0
            },
            Importe_Total_Factura: {
                required: true,
                number: true,
                min: 0
            }
        },
        messages: {
            Importe_Bruto: {
                required: "El importe bruto es obligatorio.",
                number: "Por favor, introduce un número válido.",
                min: "El importe bruto debe ser mayor o igual a 30."
            },
            Importe_IVA: {
                required: "El importe del IVA es obligatorio.",
                number: "Por favor, introduce un número válido.",
                validateIVA: "El importe del IVA debe ser el 21% del importe bruto."
            },
            Importe_IRPF: {
                required: "El importe del IRPF es obligatorio.",
                number: "Por favor, introduce un número válido.",
                min: "El importe del IRPF debe ser mayor o igual a 0."
            },
            Importe_Total_Factura: {
                required: "El importe total de la factura es obligatorio.",
                number: "Por favor, introduce un número válido.",
                min: "El importe total de la factura debe ser mayor o igual a 0."
            }
        },

        // Configuración de los estilos para mostrar los errores
        errorClass: "is-invalid",
        validClass: "is-valid",
        errorElement: "div",
        errorPlacement: function(error, element) {
            error.addClass("invalid-feedback");
            error.insertAfter(element);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).addClass(validClass).removeClass(errorClass);
        }
    });
});
