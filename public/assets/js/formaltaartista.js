$(document).ready(function () {
    // Definir el método de validación para DNI
    $.validator.addMethod("validDNI", function (value, element) {
        return this.optional(element) || /^[0-9]{8}[A-Z]$/.test(value);
    }, "Por favor ingresa un DNI válido.");

    // Definir el método de validación para NUSS
    $.validator.addMethod("validNUSS", function (value, element) {
        return this.optional(element) || /^[0-9]{9,20}$/.test(value);
    }, "Por favor ingresa un NUSS válido de entre 9 y 20 dígitos.");

    // Definir el método de validación para IBAN
    $.validator.addMethod("validIBAN", function (value, element) {
        return this.optional(element) || /^ES\d{22}$/.test(value);
    }, "Por favor ingresa un IBAN válido de España.");

    // Definir el método de validación para Código Postal
    $.validator.addMethod("validPostalCode", function (value, element) {
        return this.optional(element) || /^\d{5}$/.test(value);
    }, "Por favor ingresa un código postal válido de 5 dígitos.");

    // Inicialización de la validación del formulario para formRegistro
    $("#formRegistro").validate({
        // Reglas de validación para cada campo del formulario
        rules: {
            Nombre: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            Apellido1: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            Apellido2: {
                minlength: 2,
                maxlength: 50
            },
            Dni: {
                required: true,
                validDNI: true
            },
            Nuss: {
                required: true,
                validNUSS: true
            },
            Cuenta_Corriente: {
                required: true,
                validIBAN: true
            },
            Telefono: {
                minlength: 7,
                maxlength: 15,
                digits: true // Verifica que solo contiene números
            },
            Email: {
                minlength: 5,
                maxlength: 50,
                email: true // Verifica que es una dirección de correo válida
            },
            Calle: {
                minlength: 5,
                maxlength: 100
            },
            Numero: {
                maxlength: 10
            },
            Codigo_Postal: {
                validPostalCode: true
            },
            Localidad: {
                minlength: 2,
                maxlength: 50
            }
        },
        // Mensajes de error para cada campo y regla de validación
        messages: {
            Nombre: {
                required: "El campo Nombre es obligatorio.",
                minlength: "El campo Nombre debe tener al menos 2 caracteres.",
                maxlength: "El campo Nombre no puede tener más de 50 caracteres."
            },
            Apellido1: {
                required: "El campo Primer Apellido es obligatorio.",
                minlength: "El campo Primer Apellido debe tener al menos 2 caracteres.",
                maxlength: "El campo Primer Apellido no puede tener más de 50 caracteres."
            },
            Apellido2: {
                minlength: "El campo Segundo Apellido debe tener al menos 2 caracteres.",
                maxlength: "El campo Segundo Apellido no puede tener más de 50 caracteres."
            },
            Dni: {
                required: "El campo DNI es obligatorio.",
                validDNI: "Por favor ingresa un DNI válido."
            },
            Nuss: {
                required: "El campo NUSS es obligatorio.",
                validNUSS: "Por favor ingresa un NUSS válido de entre 9 y 20 dígitos."
            },
            Cuenta_Corriente: {
                required: "El campo Cuenta Corriente es obligatorio.",
                validIBAN: "Por favor ingresa un IBAN válido de España."
            },
            Telefono: {
                minlength: "El campo Teléfono debe tener al menos 7 dígitos.",
                maxlength: "El campo Teléfono no puede tener más de 15 dígitos.",
                digits: "Por favor ingresa un número de teléfono válido."
            },
            Email: {
                required: "El campo Email es obligatorio",
                minlength: "El campo Email debe tener al menos 5 caracteres.",
                maxlength: "El campo Email no puede tener más de 50 caracteres.",
                email: "Por favor ingresa un email válido."
            },
            Calle: {
                minlength: "El campo Calle debe tener al menos 5 caracteres.",
                maxlength: "El campo Calle no puede tener más de 100 caracteres."
            },
            Numero: {
                maxlength: "El campo Numero no puede tener más de 10 caracteres."
            },
            Codigo_Postal: {
                validPostalCode: "Por favor ingresa un código postal válido de 5 dígitos."
            },
            Localidad: {
                minlength: "El campo Localidad debe tener al menos 2 caracteres.",
                maxlength: "El campo Localidad no puede tener más de 50 caracteres."
            }
        },

        // Colocar el mensaje de error después del elemento
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        // Configuración para validar en eventos 'blur' y 'keyup'
        onfocusout: function (element) {
            $(element).valid();
        },
        onkeyup: function (element) {
            $(element).valid();
        },
        // Enviar el formulario si es válido
        submitHandler: function (form) {
            form.submit();
        }
    });
});
