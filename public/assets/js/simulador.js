/*
 * JS para el simulador
 */

//función creada "editable" sobre los cambios en cualquier campo input (del tipo que sea)
$(document).ready(function() {
    // Mensajes de ayuda
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    
    // Actualizar cálculos en tiempo real
    $(".editable").on('input change', function () {
        var dias = parseFloat($("#dias").val()) || 0;
        var base = parseFloat($("#base_imponible").val()) || 0;
        var iva = parseFloat($("#iva").val()) || 0;
        var irpf = parseFloat($("#irpf").val()) || 0;
        var cuotaServicio = 5; // Cuota de servicio fija del 5%

        var seguridadSocial = base * 0.29 * dias;
        var irpfRetencion = base * (irpf / 100);
        var ivaCalculado = base * (iva / 100);
        var cuotaServicioCalculada = base * (cuotaServicio / 100);

        var totalFactura = base + ivaCalculado;
        var importeLiquido = base - irpfRetencion - seguridadSocial - cuotaServicioCalculada;

        //toLocaleString - traducción de los cálculos a la coma como separador decimal
        //FactionDigits - formatear un número de modo que siempre tenga dos dígitos decimales
        $("#seguridad_social").val(seguridadSocial.toLocaleString('es-ES', {minimumFractionDigits: 2, maximumFractionDigits: 2}));
        $("#total").val(totalFactura.toLocaleString('es-ES', {minimumFractionDigits: 2, maximumFractionDigits: 2}));
        $("#cuota_servicio_calculada").val(cuotaServicioCalculada.toLocaleString('es-ES', {minimumFractionDigits: 2, maximumFractionDigits: 2}));
        $("#nomina").val(importeLiquido.toLocaleString('es-ES', {minimumFractionDigits: 2, maximumFractionDigits: 2}));
    });
    // validación del formulario
    $( "#simuladorForm" ).validate({
        rules: {
            base_imponible: {
                required: true,
                number: true,
                min: 30
            },
            dias: {
                required: true,
                number: true,
                min: 1,
                max: 5
            }
                    
        },
        messages: {
            base_imponible: {
                required: "Por favor, introduce la base imponible.",
                number: "Por favor, introduce un número válido.",
                min: "La base imponible debe ser una cantidad mayor que {0} euros."
            },
            dias: {
                required: "Por favor, introduce el número de días",
                number: "Por favor, introduce un número válido.",
                min: "El mínimo de días es {0}.",
                max: "El máximo de días es {0}."
            }
        },
       
    });

    
});