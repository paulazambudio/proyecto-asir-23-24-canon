<?php

/**
 * Description of ArtistaModel
 * Modelo para la tabla Artista de la base de datos "canon"
 * @author paula
 */
// Para acceder a los datos necesitamos disponer de nuestro modelo
// para ello crearemos un nuevo fichero de clase PHP

/*
 * tomado y modificado de 
 * https://codeigniter.com/user_guide/models/model.html#models
 */

// con el uso de este espacio de nombres concreto organiza y evitar conflictos de nombres con otras clases
// y la extensión de Model de CodeIgniter para heredar funcionalidades y métodos proporcionados por aquél
// 

namespace App\Models;

use CodeIgniter\Model;

class ArtistaModel extends Model {
    /*
     * ArtistaModel puede acceder a todos los métodos y propiedades definidos en la clase Model, 
     * como métodos para interactuar con la base de datos, realizar consultas, etc. 
     */

    //Define el nombre de la tabla en la base de datos que está asociada con este modelo
    protected $table = 'Artista';
    //Especifica el nombre de la columna que sirve como clave primaria en la tabla
    protected $primaryKey = 'Id_Artista';
    //booleano para saber si la columna de clave primaria se incrementará automáticamente
    protected $useAutoIncrement = true;
    //Define el tipo de datos que deben ser devueltos por los métodos de búsqueda, como find() y findAll(). 
    // J. Barrachina prefiere OBJECT así que así se queda :)
    protected $returnType = 'object';

    /*
     * en las EliminacioneSuaves los registros marcados como eliminados 
     * no se eliminan físicamente de la base de datos
     *  protected $useSoftDeletes = true;
     */
    //define los campos que pueden ser modificados en las operaciones de creación y actualización
    // en principio sin indicar esto puede funcionar igual
    protected $allowedFields = [
        'Id_Artista',
        'id_user',
        'Nombre', 'Apellido1', 'Apellido2',
        'Dni',
        'Nuss',
        'Cuenta_Corriente',
        'Telefono',
        'Email',
        'Calle', 'Numero', 'Codigo_Postal', 'Localidad'];
// Validation
    // validación obligatoria!
    protected $skipValidation = false;
    // Reglas de validación para cada campo
    protected $validationRules = [
        'Id_Artista' => 'permit_empty|numeric',
        'id_user' => 'permit_empty|numeric',
        'Nombre' => 'required|min_length[2]|max_length[50]|trim',
        'Apellido1' => 'required|min_length[2]|max_length[50]|trim',
        'Apellido2' => 'permit_empty|min_length[2]|max_length[50]|trim',
        'Dni' => 'required|regex_match[/^[0-9]{8}[A-Z]$/]|is_unique[Artista.Dni,Id_Artista,{Id_Artista}]|trim|strtoupper',
        'Nuss' => 'required|regex_match[/^[0-9]{9,20}$/]|is_unique[Artista.Nuss,Id_Artista,{Id_Artista}]|trim|strtoupper',
        'Cuenta_Corriente' => 'required|regex_match[/^ES[0-9]{2}[0-9]{4}[0-9]{4}[0-9]{2}[0-9]{10}$/]|trim|strtoupper',
        'Telefono' => 'permit_empty|min_length[7]|max_length[15]|numeric|trim',
        'Email' => 'permit_empty|min_length[5]|max_length[50]|valid_email|trim',
        'Calle' => 'permit_empty|min_length[5]|max_length[100]|trim',
        'Numero' => 'permit_empty|max_length[10]|trim',
        'Codigo_Postal' => 'permit_empty|min_length[5]|max_length[10]|trim|strtoupper',
        'Localidad' => 'permit_empty|min_length[2]|max_length[50]|trim'
    ];
    /* - trim: eliminar espacios en blanco al inicio y al final
     * - strtoupper: devuelve el string con todos los caracteres alfabéticos convertidos a mayúsculas
     * - is_unique[Artista.Dni,Id_Artista,{Id_Artista}]: "marcador de posición" para permitir la edición conservando unicidad,
     *      para excluir el registro actual de la comprobación al validar
     * - permit_empty en Id_Artista está pensado para la creación, porque en la base de datos es un INT AUTO_INCREMENT - en edición esto debería pulirse
     */
    //Mensajes de validación 
    protected $validationMessages = [
        'Nombre' => [
            'required' => 'El campo Nombre es obligatorio.',
            'min_length' => 'El campo Nombre debe tener al menos 2 caracteres.',
            'max_length' => 'El campo Nombre no puede tener más de 50 caracteres.'
        ],
        'Apellido1' => [
            'required' => 'El campo Primer Apellido es obligatorio.',
            'min_length' => 'El campo Primer Apellido debe tener al menos 2 caracteres.',
            'max_length' => 'El campo Primer Apellido no puede tener más de 50 caracteres.'
        ],
        'Apellido2' => [
            'min_length' => 'El campo Segundo Apellido debe tener al menos 2 caracteres.',
            'max_length' => 'El campo Segundo Apellido no puede tener más de 50 caracteres.'
        ],
        'Dni' => [
            'required' => 'El campo DNI es obligatorio.',
            'regex_match' => 'El campo DNI debe tener 8 dígitos seguidos de una letra mayúscula.',
            'is_unique' => 'El DNI ingresado ya está registrado.'
        ],
        'Nuss' => [
            'required' => 'El campo NUSS es obligatorio.',
            'regex_match' => 'El campo NUSS debe contener entre 9 y 20 dígitos numéricos.',
            'is_unique' => 'El NUSS ingresado ya está registrado.'
        ],
        'Cuenta_Corriente' => [
            'required' => 'El campo Cuenta Corriente es obligatorio.',
            'regex_match' => 'El campo Cuenta Corriente debe comenzar con ES, seguido de 22 dígitos numéricos.'
        ],
        'Telefono' => [
            'min_length' => 'El campo Teléfono debe tener al menos 7 dígitos.',
            'max_length' => 'El campo Teléfono no puede tener más de 15 dígitos.',
            'numeric' => 'El campo Teléfono debe contener solo números.'
        ],
        'Email' => [
            'min_length' => 'El campo Email debe tener al menos 5 caracteres.',
            'max_length' => 'El campo Email no puede tener más de 50 caracteres.',
            'valid_email' => 'El campo Email debe contener una dirección de correo electrónico válida.'
        ],
        'Calle' => [
            'min_length' => 'El campo Calle debe tener al menos 5 caracteres.',
            'max_length' => 'El campo Calle no puede tener más de 100 caracteres.'
        ],
        'Numero' => [
            'max_length' => 'El campo Número no puede tener más de 10 caracteres.'
        ],
        'Codigo_Postal' => [
            'regex_match' => 'El campo Código Postal debe contener 5 dígitos.'
        ],
        'Localidad' => [
            'min_length' => 'El campo Localidad debe tener al menos 2 caracteres.',
            'max_length' => 'El campo Localidad no puede tener más de 50 caracteres.'
        ]
    ];
    protected $cleanValidationRules = true;
}
