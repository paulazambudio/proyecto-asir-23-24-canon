<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;

/**
 * Description of FacturaModel
 *
 * @author paula
 */
class FacturaModel extends \CodeIgniter\Model {
    //Define el nombre de la tabla en la base de datos que está asociada con este modelo
    protected $table = 'Factura';
     //Especifica el nombre de la columna que sirve como clave primaria en la tabla
    protected $primaryKey = 'Id_Factura';
    //booleano para saber si la columna de clave primaria se incrementará automáticamente
    protected $useAutoIncrement = true;
     //Define el tipo de datos que deben ser devueltos por los métodos de búsqueda, como find() y findAll(). 
    protected $returnType = 'object';
    //protected $useSoftDeletes = true;
    protected $allowedFields = [
        'Fecha_Emision', 
        'Estado', 
        'Importe_Bruto', 
        'Importe_IVA', 
        'Importe_IRPF', 
        'Porcentaje_IVA', 
        'Porcentaje_IRPF', 
        'Importe_Total_Factura', 
        'Id_Solicitud_Factura', 
        'Id_Promotor_Factura'
    ];
    // Validation
    // validación obligatoria!
    protected $skipValidation = false;
    
    // Reglas de validación para cada campo
    protected $validationRules = [
        'Fecha_Emision' => 'required|valid_date',
        'Estado' => 'required|max_length[50]',
        'Importe_Bruto' => 'required|decimal|greater_than[0]',
        'Importe_IVA' => 'required|decimal|greater_than_equal_to[0]',
        'Importe_IRPF' => 'required|decimal|greater_than_equal_to[0]',
        'Porcentaje_IVA' => 'required|decimal|greater_than_equal_to[0]|less_than_equal_to[100]',
        'Porcentaje_IRPF' => 'required|decimal|greater_than_equal_to[0]|less_than_equal_to[100]',
        'Importe_Total_Factura' => 'required|decimal|greater_than[0]',
        'Id_Solicitud_Factura' => 'permit_empty|integer|is_not_unique[Solicitud.Id_Solicitud]',
        'Id_Promotor_Factura' => 'permit_empty|integer|is_not_unique[Promotor.Id_Promotor]'
    ];
    protected $validationMessages = [
        'Fecha_Emision' => [
            'required' => 'La fecha de emisión es obligatoria.',
            'valid_date' => 'Por favor, ingrese una fecha válida.'
        ],
        'Estado' => [
            'required' => 'El estado es obligatorio.',
            'max_length' => 'El estado no puede tener más de 50 caracteres.'
        ],
        'Importe_Bruto' => [
            'required' => 'El importe bruto es obligatorio.',
            'decimal' => 'El importe bruto debe ser un número decimal.',
            'greater_than' => 'El importe bruto debe ser mayor que 0.'
        ],
        'Importe_IVA' => [
            'required' => 'El importe del IVA es obligatorio.',
            'decimal' => 'El importe del IVA debe ser un número decimal.',
            'greater_than_equal_to' => 'El importe del IVA debe ser mayor o igual a 0.'
        ],
        'Importe_IRPF' => [
            'required' => 'El importe del IRPF es obligatorio.',
            'decimal' => 'El importe del IRPF debe ser un número decimal.',
            'greater_than_equal_to' => 'El importe del IRPF debe ser mayor o igual a 0.'
        ],
        'Porcentaje_IVA' => [
            'required' => 'El porcentaje del IVA es obligatorio.',
            'decimal' => 'El porcentaje del IVA debe ser un número decimal.',
            'greater_than_equal_to' => 'El porcentaje del IVA debe ser mayor o igual a 0.',
            'less_than_equal_to' => 'El porcentaje del IVA debe ser menor o igual a 100.'
        ],
        'Porcentaje_IRPF' => [
            'required' => 'El porcentaje de IRPF es obligatorio.',
            'decimal' => 'El porcentaje de IRPF debe ser un número decimal.',
            'greater_than_equal_to' => 'El porcentaje de IRPF debe ser mayor o igual a 0.',
            'less_than_equal_to' => 'El porcentaje de IRPF debe ser menor o igual a 100.'
        ],
        'Importe_Total_Factura' => [
            'required' => 'El importe total de la factura es obligatorio.',
            'decimal' => 'El importe total de la factura debe ser un número decimal.',
            'greater_than' => 'El importe total de la factura debe ser mayor que 0.'
        ],
        'Id_Solicitud_Factura' => [
            'integer' => 'El ID de la solicitud de factura debe ser un número entero.',
            'is_not_unique' => 'El ID de la solicitud debe existir en la tabla de solicitudes.'
        ],
        'Id_Promotor_Factura' => [
            'integer' => 'El ID del promotor de la factura debe ser un número entero.',
            'is_not_unique' => 'El ID del promotor debe existir en la tabla de promotores.'
        ]
    ];
}
