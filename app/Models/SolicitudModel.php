<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;

/**
 * Description of SolicitudModel
 *
 * @author paula
 */
class SolicitudModel extends \CodeIgniter\Model {

    protected $table = 'Solicitud';
    protected $primaryKey = 'Id_Solicitud';
    protected $returnType = 'object';
    protected $allowedFields = [
        'Fecha_Solicitud', 'Fecha_Inicio', 'Fecha_Fin', 'Ubicacion_Evento',
        'Descripcion', 'Importe_Bruto', 'Importe_IVA', 'Importe_IRPF',
        'Porcentaje_IVA', 'Porcentaje_IRPF', 'Importe_Nomina',
        'Cuota_Servicio', 'Id_Artista_Solicitud', 'Id_Promotor_Solicitud'
    ];
    protected $skipValidation = false;
    //revisar
    protected $validationRules = [
        'Fecha_Inicio' => 'required|valid_date',
        'Fecha_Fin' => 'required|valid_date',
        'Ubicacion_Evento' => 'permit_empty|string|max_length[255]',
        'Descripcion' => 'permit_empty|string',
        'Importe_Bruto' => 'required|decimal|greater_than[0]',
        'Importe_IVA' => 'required|decimal|greater_than_equal_to[0]',
        'Importe_IRPF' => 'required|decimal|greater_than_equal_to[0]',
        'Porcentaje_IVA' => 'required|equals[21]',
        'Porcentaje_IRPF' => 'required|decimal|greater_than_equal_to[0]|less_than_equal_to[100]',
        'Importe_Nomina' => 'required|decimal|greater_than[0]',
        'Cuota_Servicio' => 'required|decimal|greater_than_equal_to[0]',
        'Id_Artista_Solicitud' => 'required|integer|is_not_unique[Artista.Id_Artista]',
        'Id_Promotor_Solicitud' => 'required|integer|is_not_unique[Promotor.Id_Promotor]'
    ];
    //revisar
    protected $validationMessages = [
        'Ubicacion_Evento' => [
            'max_length' => 'La ubicación del evento no puede exceder los 255 caracteres.'
        ],
        'Importe_Bruto' => [
            'required' => 'El importe bruto es obligatorio.',
            'decimal' => 'El importe bruto debe ser un número decimal.',
            'greater_than' => 'El importe bruto debe ser mayor que 30.'
        ],
        'Importe_IVA' => [
            'required' => 'El importe del IVA es obligatorio.',
            'equals' => 'El porcentaje de IVA debe ser exactamente 21.'
        ],
        'Importe_IRPF' => [
            'required' => 'El importe del IRPF es obligatorio.',
            'decimal' => 'El importe del IRPF debe ser un número decimal.',
            'greater_than_equal_to' => 'El importe del IRPF debe ser mayor o igual a 0.'
        ],
        'Porcentaje_IVA' => [
            'required' => 'El porcentaje de IVA es obligatorio.',
            'decimal' => 'El porcentaje de IVA debe ser un número decimal.',
            'greater_than_equal_to' => 'El porcentaje de IVA debe ser mayor o igual a 0.',
            'less_than_equal_to' => 'El porcentaje de IVA debe ser menor o igual a 100.'
        ],
        'Porcentaje_IRPF' => [
            'required' => 'El porcentaje de IRPF es obligatorio.',
            'decimal' => 'El porcentaje de IRPF debe ser un número decimal.',
            'greater_than_equal_to' => 'El porcentaje de IRPF debe ser mayor o igual a 0.',
            'less_than_equal_to' => 'El porcentaje de IRPF debe ser menor o igual a 100.'
        ],
        'Importe_Nomina' => [
            'required' => 'El importe de la nómina es obligatorio.',
            'decimal' => 'El importe de la nómina debe ser un número decimal.',
            'greater_than' => 'El importe de la nómina debe ser mayor que 0.'
        ],
        'Cuota_Servicio' => [
            'required' => 'La cuota del servicio es obligatoria.',
            'decimal' => 'La cuota del servicio debe ser un número decimal.',
            'greater_than_equal_to' => 'La cuota del servicio debe ser mayor o igual a 0.'
        ],
        'Id_Artista_Solicitud' => [
            'required' => 'El ID del artista es obligatorio.',
            'integer' => 'El ID del artista debe ser un número entero.',
            'is_not_unique' => 'El ID del artista debe existir en la tabla de artistas.'
        ],
        'Id_Promotor_Solicitud' => [
            'required' => 'El ID del promotor es obligatorio.',
            'integer' => 'El ID del promotor debe ser un número entero.',
            'is_not_unique' => 'El ID del promotor debe existir en la tabla de promotores.'
        ]
    ];

    // Método para obtener las solicitudes del artista actual
    public function obtenerSolicitudesParaArtista($idArtista) {
        return $this->where('Id_Artista_Solicitud', $idArtista)->findAll();
    }

    public function buscarSolicitudesSinFactura($texto = '') {
        // Subconsulta para obtener IDs de solicitudes que ya tienen una factura asignada
        $subQuery = $this->db->table('Factura')
                ->select('Id_Solicitud_Factura')
                ->getCompiledSelect();

        // Construir la consulta principal para obtener solicitudes sin factura
        $builder = $this->db->table('Solicitud')
                ->select('Solicitud.*, Promotor.Razon_Social as Nombre_Promotor, 
                  Artista.Nombre as Nombre_Artista, Artista.Apellido1 as Apellido1_Artista, Artista.Apellido2 as Apellido2_Artista')
                ->join('Promotor', 'Promotor.Id_Promotor = Solicitud.Id_Promotor_Solicitud', 'left')
                ->join('Artista', 'Artista.Id_Artista = Solicitud.Id_Artista_Solicitud', 'left')
                ->where("Solicitud.Id_Solicitud NOT IN ($subQuery)", null, false);

        // Si hay texto de búsqueda, añadir condiciones para filtrar por ID o Descripción
        if (!empty($texto)) {
            $builder->groupStart()
                    ->like('Solicitud.Id_Solicitud', $texto)
                    ->orLike('Solicitud.Descripcion', $texto)
                    ->groupEnd();
        }

        // Ejecutar la consulta y devolver los resultados
        return $builder->get()->getResult();
    }
}
