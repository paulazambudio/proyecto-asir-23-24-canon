<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;
/**
 * Description of PromotorModel
 *
 * @author paula
 */
class PromotorModel extends Model {
    //Define el nombre de la tabla en la base de datos que está asociada con este modelo
    protected $table = 'Promotor';
    protected $primaryKey = 'Id_Promotor';

    protected $useAutoIncrement = true;
    
    protected $returnType = 'object';
    
    protected $allowedFields = [
        'Id_Promotor', 
        'Tipo_Entidad',
        'Nombre',
        'Apellido1', 'Apellido2', 
        'Razon_Social',
        'CIF',
        'Telefono',
        'Email',
        'Calle', 'Numero', 
        'Codigo_Postal', 
        'Localidad'
        ];
    
    // Validation
    protected $skipValidation = false;
    
    // Reglas de validación para cada campo
    protected $validationRules = [
        'Tipo_Entidad' => 'required|in_list[Fisica,Juridica]',
        'Nombre' => 'permit_empty|max_length[50]',
        'Apellido1' => 'permit_empty|max_length[50]',
        'Apellido2' => 'permit_empty|max_length[50]',
        'Razon_Social' => 'permit_empty|max_length[100]',
        'CIF' => 'required|min_length[9]|max_length[20]|is_unique[Promotor.CIF,Id_Promotor,{Id_Promotor}]|trim|strtoupper',
        'Telefono' => 'permit_empty|min_length[7]|max_length[15]|numeric|trim',
        'Email' => 'permit_empty|min_length[5]|max_length[50]|valid_email|trim',
        'Calle' => 'permit_empty|min_length[5]|max_length[100]|trim',
        'Numero' => 'permit_empty|max_length[10]|trim',
        'Codigo_Postal' => 'permit_empty|min_length[5]|max_length[10]|trim|strtoupper',
        'Localidad' => 'permit_empty|min_length[2]|max_length[50]|trim',
        'chk_tipo_entidad_fj' => 'validate_tipo_entidad_fj[Tipo_Entidad,Nombre,Apellido1,Apellido2,Razon_Social]'
    ];
    
    protected $validationMessages = [
        'Tipo_Entidad' => [
            'in_list' => 'El campo Tipo_Entidad debe ser "Fisica" o "Juridica".'
        ],
        'CIF' => [
            'is_unique' => 'Este CIF ya está registrado.'
        ],
        'chk_tipo_entidad_fj' => [
            'validate_tipo_entidad_fj' => 'Los campos no cumplen con las restricciones para el Tipo_Entidad especificado.'
        ]
    ];

    // Método para validar el CHECK de la BD
    public function validate_tipo_entidad_fj(string $str, string $fields, array $data): bool
    {
        if ($data['Tipo_Entidad'] == 'Fisica') {
            return !empty($data['Nombre']) && !empty($data['Apellido1']) && !empty($data['Apellido2']) && empty($data['Razon_Social']);
        } elseif ($data['Tipo_Entidad'] == 'Juridica') {
            return !empty($data['Razon_Social']) && empty($data['Nombre']) && empty($data['Apellido1']) && empty($data['Apellido2']);
        }
        return false;
    }
    
    public function buscar($texto){
        $resultado = $this->builder()
                ->where("CONCAT(COALESCE(CIF,''),' ',COALESCE(Razon_Social,''),' ',COALESCE(Apellido1,'')) LIKE '%$texto%'")
                ->get()
                ->getResult();
                //->getCompiledSelect();
        return $resultado;
    }
}
