<!-- Extendemos la plantilla deseada -->
<?= $this->extend('plantillas/adminlte') ?>

<!-- CSS opcional -->
<?= $this->section('css') ?>
<?= $this->include('common/datatables') ?>
<?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>


<!-- Título de la Página -->
<?= $this->section('page_title') ?>
Área personal
<?= $this->endSection() ?>

<?= $this->section('side_bar') ?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?= $this->include('common/sidebar') ?>
</aside>
<?= $this->endSection() ?>

<!-- Título de la card -->
<?= $this->section('subtitle') ?>
¡Bienvenid@ a Canon!
<?= $this->endSection() ?>


<!-- contenido principal dentro de card0 -->
<?= $this->section('content') ?>
<h4 class="text-success">Completa el formulario con tus datos para poder realizar solicitudes:</h4>
<p>Necesitamos tus datos personales e información económica para incluirla en nuestra base de datos. 
    Con ello, podremos a trabajar juntos. </p>
<div class="row">
    <div class="col-md-4">
        <a href="<?= site_url('/registrar') ?>" class="text-decoration-none">
            <div class="info-box">
                <span class="info-box-icon bg-success"><i class="fas fa-user-plus"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Date de alta</span>
                    <span class="info-box-number">Completa el registro</span>
                </div>
            </div>
        </a>
    </div>
</div>
<h4 class="text-maroon">Puedes hacer números en nuestra calculadora:</h4>
<p>¿No sabes cuánto dinero te llegará al final? ¿Quieres saber cuánto cuesta cada apartado? 
    Puedes verlo de forma detallada e interactiva en nuestra sencilla calculadora:</p>
<div class="row">
    <div class="col-md-4">
        <a href="<?= site_url('/simulador') ?>" class="text-decoration-none">
            <div class="info-box">
                <span class="info-box-icon bg-maroon"><i class="fas fa-hand-holding-dollar"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Simulador</span>
                    <span class="info-box-number">¿Cuánto cobraré?</span>
                </div>
            </div>
        </a>
    </div>
</div>
<h4 class="text-lightblue">¿Tienes una actuación? ¡Vamos a gestionarla!</h4>
<p>Ten preparada toda la información económica y fiscal de la actuación para poder completar el formulario de Solicitud. </p>
<div class="row">
    <div class="col-md-4">
        <a href="<?= site_url('/nueva_solicitud') ?>" class="text-decoration-none">
            <div class="info-box">
                <span class="info-box-icon bg-lightblue"><i class="fas fa-file-export"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Nueva solicitud</span>
                    <span class="info-box-number">Crear nueva solicitud de alta en la Seguridad Social</span>
                </div>
            </div>
        </a>
    </div>
</div>


<h4 class="text-info">Revisa las dudas de gente como tú en nuestras FAQ:</h4>
<p>Hemos preparado una lista con las cuestiones más habituales para que puedas quedarte tranquil@:</p>
<div class="row"> 
    <div class="col-md-4">
        <a href="<?= site_url('/preguntasfrecuentes') ?>" class="text-decoration-none">
            <div class="info-box">
                <span class="info-box-icon bg-info"><i class="fas fa-file-circle-question"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">¿Dudas?</span>
                    <span class="info-box-number">Preguntas frecuentes</span>
                </div>
            </div>
        </a>
    </div>
</div>


<?= $this->endSection() ?>

<!-- JS opcional -->
<?= $this->section('js') ?>
<?= $this->include('common/datatables') ?>
<?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>