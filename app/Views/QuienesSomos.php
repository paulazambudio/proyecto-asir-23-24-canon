<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */
?>


<?= $this->extend('plantillas/adminlte_navbar') ?>

<!-- CSS -->
<?= $this->section('css') ?>
<?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
Quiénes somos
<?= $this->endSection() ?>

<?= $this->section('subtitle') ?>
Sobre nosotros
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<style>
    .profile-img {
        width: 6em;
        height: 6em;
        border-radius: 50%;
    }

</style>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Aquí va el contenido de "Quiénes Somos" -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Nuestra Historia</h3>
                    </div>
                    <div class="card-body">
                        <p>
                            Nuestra empresa fue fundada en 2012 con la misión de facilitar la gestión laboral y fiscal de artistas, técnicos y otros profesionales del sector. 
                            Desde nuestros inicios, hemos trabajado incansablemente para proporcionar un servicio eficiente y seguro 
                            que permita a los artistas emitir facturas y gestionar sus relaciones laborales sin la necesidad de darse de alta como autónomos.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Nuestra Misión</h3>
                    </div>
                    <div class="card-body">
                        <p>
                            Nuestra misión es actuar como intermediarios confiables entre artistas y promotores, 
                            facilitando la gestión administrativa, laboral y fiscal para que los artistas puedan 
                            centrarse en lo que mejor saben hacer: crear y actuar.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title">Nuestros Valores</h3>
                    </div>
                    <div class="card-body">
                        <ul>
                            <li>Eficiencia: Optimización de procesos para reducir la carga administrativa.</li>
                            <li>Seguridad: Manejo seguro de información sensible.</li>
                            <li>Transparencia: Claridad en nuestras operaciones y servicios.</li>
                            <li>Compromiso: Dedicación hacia nuestros clientes para garantizar su satisfacción.</li>
                            <li>Innovación: Implementación de soluciones tecnológicas avanzadas para mejorar nuestros servicios.</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card card-warning">
                    <div class="card-header">
                        <h3 class="card-title">Nuestros expertos</h3>

                    </div>
                    <div class="card-body">
                        <p>
                            Contamos con un equipo multidisciplinario de profesionales altamente capacitados en áreas de gestión administrativa y asesoría fiscal y laboral,
                            comprometidos en ofrecer el mejor servicio a nuestros clientes.
                        </p>
                        <div class="row">
                            <!-- Tarjeta de perfil -->
                            <div class="col-md-4 col-sm-6">
                                <div class="card">
                                    <div class="card-header text-muted border-bottom-0">
                                        Departamento Laboral
                                    </div>
                                    <div class="card-body pt-0">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="lead"><b>Alejandra Pérez</b></h2>
                                                <p class="text-muted text-sm"><b></b> Graduada en ADE </p>
                                            </div>
                                            <div class="col-5 text-center">
                                                <img class="card-img-top profile-img img-circle img-fluid" src="assets/images/099999.jpg" alt="AlejandraPerez">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Fin de la tarjeta de perfil -->
                            <div class="col-md-4 col-sm-6">
                                <div class="card">
                                    <div class="card-header text-muted border-bottom-0">
                                        Departamento Laboral
                                    </div>
                                    <div class="card-body pt-0">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="lead"><b>Gema García</b></h2>
                                                <p class="text-muted text-sm"><b></b>Licenciada en Derecho</p>
                                            </div>
                                            <div class="col-5 text-center">
                                                <img class="card-img-top profile-img img-circle img-fluid" src="assets/images/gemagarcia.jpg" alt="GemaGarcia">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Fin de la tarjeta de perfil -->
                            <div class="col-md-4 col-sm-6">
                                <div class="card">
                                    <div class="card-header text-muted border-bottom-0">
                                        Departamento Fiscal
                                    </div>
                                    <div class="card-body pt-0">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="lead"><b>José Luis Martínez</b></h2>
                                                <p class="text-muted text-sm"><b></b> Licenciado en Economía</p>
                                            </div>
                                            <div class="col-5 text-center">
                                                <img class="card-img-top profile-img img-circle img-fluid" src="assets/images/joseluismartinez.jpg" alt="JLMartinez">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- Tarjeta de perfil -->
                            <div class="col-md-4 col-sm-6">
                                <div class="card">
                                    <div class="card-header text-muted border-bottom-0">
                                        Departamento Administrativo
                                    </div>
                                    <div class="card-body pt-0">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="lead"><b>Pilar López</b></h2>
                                                <p class="text-muted text-sm"><b></b> Ciclo en Administración y Finanzas </p>
                                            </div>
                                            <div class="col-5 text-center">
                                                <img class="card-img-top profile-img img-circle img-fluid" src="assets/images/pilarlopez.jpg" alt="PilarLopez">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Fin de la tarjeta de perfil -->
                            <div class="col-md-4 col-sm-6">
                                <div class="card">
                                    <div class="card-header text-muted border-bottom-0">
                                        Departamento Administrativo
                                    </div>
                                    <div class="card-body pt-0">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="lead"><b>Francisco Díaz</b></h2>
                                                <p class="text-muted text-sm"><b></b>Ciclo en Administración y Finanza</p>
                                            </div>
                                            <div class="col-5 text-center">
                                                <img class="card-img-top profile-img img-circle img-fluid" src="assets/images/pacodiaz.jpg" alt="PacoDiaz">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Fin de la tarjeta de perfil -->
                            <div class="col-md-4 col-sm-6">
                                <div class="card">
                                    <div class="card-header text-muted border-bottom-0">
                                        CEO y músico
                                    </div>
                                    <div class="card-body pt-0">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="lead"><b>Federico Jiménez</b></h2>
                                                <p class="text-muted text-sm"><b></b> Licenciado en Derecho</p>
                                            </div>
                                            <div class="col-5 text-center">
                                                <img class="card-img-top profile-img img-circle img-fluid" src="assets/images/federicojimenez.jpg" alt="JLMartinez">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<?= $this->endSection() ?>

<?= $this->section('js') ?>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- Bootstrap JS con Popper.js incluido -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
<?= $this->endSection() ?>