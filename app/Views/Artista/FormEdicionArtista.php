<!-- Utiliza la plantilla y creamos el formulario con form helper  -->

<!-- Extendemos la plantilla de formulario de edición que hemos creado antes-->
<?= $this->extend('plantillas/adminlte_navbar') ?>

<?= $this->section('title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
    <h2><?= $titulo ?></h2>

    <?php if (isset($errors) && !empty($errors)): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errors as $error): ?>
                    <li><?= $error ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?= form_open('artista/editar/'.$artista->Id_Artista, ['id' => 'formEdicion']) ?>
    <div class="form-group">
        <?= form_label('Nombre:', 'Nombre') ?>
        <?= form_input('Nombre', set_value('Nombre', esc($artista->Nombre)), ['id' => 'Nombre', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('1er Apellido:', 'Apellido1') ?>
        <?= form_input('Apellido1', set_value('Apellido1', esc($artista->Apellido1)), ['id' => 'Apellido1', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('2º Apellido:', 'Apellido2') ?>
        <?= form_input('Apellido2', set_value('Apellido2', esc($artista->Apellido2)), ['id' => 'Apellido2', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Email:', 'Email') ?>
        <?= form_input('Email', set_value('Email', esc($artista->Email)), ['id' => 'Email', 'class' => 'form-control', 'type' => 'email', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('DNI:', 'dni') ?>
        <?= form_input('Dni', set_value('Dni', esc($artista->Dni)), ['id' => 'dni', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('NUSS:', 'nuss') ?>
        <?= form_input('Nuss', set_value('Nuss', esc($artista->Nuss)), ['id' => 'nuss', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Cuenta Corriente:', 'cuenta_corriente') ?>
        <?= form_input('Cuenta_Corriente', set_value('Cuenta_Corriente', esc($artista->Cuenta_Corriente)), ['id' => 'cuenta_corriente', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Teléfono:', 'telefono') ?>
        <?= form_input('Telefono', set_value('Telefono', esc($artista->Telefono)), ['id' => 'telefono', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Calle:', 'calle') ?>
        <?= form_input('Calle', set_value('Calle', esc($artista->Calle)), ['id' => 'calle', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Número:', 'numero') ?>
        <?= form_input('Numero', set_value('Numero', esc($artista->Numero)), ['id' => 'numero', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Código Postal:', 'codigo_postal') ?>
        <?= form_input('Codigo_postal', set_value('Codigo_Postal', esc($artista->Codigo_Postal)), ['id' => 'codigo_postal', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Localidad:', 'localidad') ?>
        <?= form_input('Localidad', set_value('Localidad', esc($artista->Localidad)), ['id' => 'localidad', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Guardar</button>
        <a href="<?= base_url('/artistas') ?>" class="btn btn-danger"  data-toggle="tooltip"
           data-placement="right" title="Volver a la tabla de Artistas SIN GUARDAR los cambios de la edición">
            Cancelar
        </a>
    </div>
    <?= form_close() ?>
</div>

<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- jQuery Validation Plugin -->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<!-- Bootstrap JS con Popper.js incluido -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>

<!-- Archivo JS de validación -->
<script src="<?= base_url('assets/js/validacionartista_js.js') ?>"></script>

<?= $this->endSection() ?>