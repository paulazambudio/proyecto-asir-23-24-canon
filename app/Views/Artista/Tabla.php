<?php
/* * *****************************************************************************
 * Ejemplo de VISTA que utiliza la PLANTILLA de adminlte 
 * que está en app\Views\plantillas\adminlte.php
 * **************************************************************************** */
?>
<!-- Extendemos la plantilla deseada -->
<?= $this->extend('plantillas/adminlte') ?>

<!-- CSS DataTables -->
<?= $this->section('css') ?>
<?= $this->include('common/datatables') ?>
<?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>

<!--  Título de la página, y la llamaremos en la plantilla -->
<?= $this->section('page_title') ?>
<?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('side_bar') ?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?= $this->include('common/sidebar') ?>
</aside>
<?= $this->endSection() ?>

<!-- Card -->
<?= $this->section('subtitle') ?>
Tabla de artistas registrados
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="table-responsive">
    <table id="myTable" class="table table-stripped table-bordered" style="width: 100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Apellidos</th>
                <th>Nombre</th>
                <th>E-mail</th>
                <th>Teléfono</th>
                <th>Calle</th>
                <th>Código Postal</th>
                <th>Localidad</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($artistas as $artista): ?>
                <tr>
                    <td><?= $artista->Id_Artista ?></td>
                    <td><?= $artista->Apellido1 ?> <?= $artista->Apellido2 ?></td>
                    <td><?= $artista->Nombre ?></td>
                    <td><?= $artista->Email ?></td>
                    <td><?= $artista->Telefono ?></td>
                    <td><?= $artista->Calle ?>, <?= $artista->Numero ?></td>
                    <td><?= $artista->Codigo_Postal ?></td>
                    <td><?= $artista->Localidad ?></td>
                    <td class="text-center">
                        <a href="<?= site_url('artista/editar/' . $artista->Id_Artista) ?>" 
                           data-toggle="tooltip" title="Editar Artista" class="mr-2">
                            <span class="fa-solid fa-pencil text-info">
                            </span>
                        </a>
                        <a href="<?= site_url('artista/borrar/' . $artista->Id_Artista) ?>" 
                           onclick="return confirm('¿Estás seguro de que quieres borrar el Artista seleccionado? Esta acción no se puede deshacer')"
                           data-toggle="tooltip" title="Borrar Artista" class="ml-2">
                            <span class="fa fa-trash text-danger">
                            </span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->endSection() ?>

<!-- JS DataTables -->
<?= $this->section('js') ?>
    <?= $this->include('common/datatables') ?>
    <?= $this->include('common/bootstrap') ?>

    <script>
    // Inicializar tooltips
        $('[data-toggle="tooltip"]').tooltip();
    </script>
<?= $this->endSection() ?>

