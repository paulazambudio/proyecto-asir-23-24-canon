
<?= $this->extend('plantillas/adminlte_navbar') ?>

<?= $this->section('title') ?>
<?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('css') ?>
<!-- CSS personalizado para errores de validacion en public/assets/css -->
<link rel="stylesheet" href="<?= base_url('assets/css/validacionform_css.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
    <h2><?= $titulo ?></h2>

    <!-- ALERTAS para manejo de errores -->
    <?php if (isset($errors)): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errors as $error): ?>
                    <li><?= $error ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <!-- el formulario y su id para el JS -->
    <?= form_open('artista/procesarFormularioRegistro', ['id' => 'formRegistro']) ?>
    <div class="form-group">
        <?= form_label('Nombre:', 'Nombre') ?>
        <?= form_input('Nombre', set_value('Nombre', ''), ['id' => 'Nombre', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('1er Apellido:', 'Apellido1') ?>
        <?= form_input('Apellido1', set_value('Apellido1', ''), ['id' => 'Apellido1', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('2º Apellido:', 'Apellido2') ?>
        <?= form_input('Apellido2', set_value('Apellido2', ''), ['id' => 'Apellido2', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Email:', 'Email') ?>
        <?= form_input('Email', set_value('Email', ''), ['id' => 'Email', 'class' => 'form-control', 'type' => 'Email', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('DNI:', 'Dni') ?>
        <?= form_input('Dni', set_value('Dni', ''), ['id' => 'Dni', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('NUSS:', 'Nuss') ?>
        <?= form_input('Nuss', set_value('Nuss', ''), ['id' => 'Nuss', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Cuenta Corriente:', 'Cuenta_Corriente') ?>
        <?= form_input('Cuenta_Corriente', set_value('Cuenta_Corriente', ''), ['id' => 'Cuenta_Corriente', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Teléfono:', 'Telefono') ?>
        <?= form_input('Telefono', set_value('Telefono', ''), ['id' => 'Telefono', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Calle:', 'Calle') ?>
        <?= form_input('Calle', set_value('Calle', ''), ['id' => 'Calle', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Número:', 'Numero') ?>
        <?= form_input('Numero', set_value('Numero', ''), ['id' => 'Numero', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Código Postal:', 'Codigo_Postal') ?>
        <?= form_input('Codigo_Postal', set_value('Codigo_Postal', ''), ['id' => 'Codigo_Postal', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Localidad:', 'Localidad') ?>
        <?= form_input('Localidad', set_value('Localidad', ''), ['id' => 'Localidad', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Enviar</button>
        <a href="<?= previous_url() ?>" class="btn btn-danger">Cancelar</a>
    </div>
</div>
<?= form_close() ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- jQuery Validation Plugin -->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<!-- Bootstrap JS con Popper.js incluido -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
<!-- JS de validación en public/assets/js por higienizar -->
<script src="<?= base_url('assets/js/formaltaartista.js') ?>"></script>
<?= $this->endSection() ?>