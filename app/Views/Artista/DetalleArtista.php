<?= $this->extend('plantillas/adminlte_navbar') ?>

<?= $this->section('title') ?>
    Detalles del artista registrado
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
    <h2>Información de los campos insertados</h2>

    <?php if (session()->getFlashdata('success')): ?>
        <div class="alert alert-success">
            <?= session()->getFlashdata('success') ?>
        </div>
    <?php endif; ?>

    <?php if (isset($artista)): ?>
        <div class="alert alert-info">
            <h4>Datos del Artista Registrado:</h4>
            <p><strong>Nombre:</strong> <?= esc($artista->Nombre) ?></p>
            <p><strong>Primer Apellido:</strong> <?= esc($artista->Apellido1) ?></p>
            <p><strong>Segundo Apellido:</strong> <?= esc($artista->Apellido2) ?></p>
            <p><strong>DNI:</strong> <?= esc($artista->Dni) ?></p>
            <p><strong>NUSS:</strong> <?= esc($artista->Nuss) ?></p>
            <p><strong>Cuenta Corriente:</strong> <?= esc($artista->Cuenta_Corriente) ?></p>
            <p><strong>Teléfono:</strong> <?= esc($artista->Telefono) ?></p>
            <p><strong>Email:</strong> <?= esc($artista->Email) ?></p>
            <p><strong>Calle:</strong> <?= esc($artista->Calle) ?></p>
            <p><strong>Número:</strong> <?= esc($artista->Numero) ?></p>
            <p><strong>Código Postal:</strong> <?= esc($artista->Codigo_Postal) ?></p>
            <p><strong>Localidad:</strong> <?= esc($artista->Localidad) ?></p>
        </div>
    <?php else: ?>
        <div class="alert alert-danger">
            No se encontró la información del artista.
        </div>
    <?php endif; ?>

    <a href="<?= site_url('/registrar') ?>" class="btn btn-danger">Registrar otro artista</a>
    <a href="<?= site_url('/') ?>" class="btn btn-secondary">Inicio</a>
</div>

<?= $this->endSection() ?>
