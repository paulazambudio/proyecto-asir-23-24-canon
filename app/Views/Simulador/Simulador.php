<?php /**
 * Description of Simulador
 * Calculadora de salario de 1 artista
 * @author paula
 */ ?>

<?= $this->extend('plantillas/adminlte_navbar') ?>

<!-- hoja de estilo Calculadora -->
<?= $this->section('css') ?>
    <link rel="stylesheet" href="<?= base_url('assets/css/simulador.css') ?>">
<?= $this->endSection() ?>

<!-- validación -->
<?= $this->section('js') ?>
<script src="<?= base_url('assets/js/simulador.js') ?>"></script>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- jQuery Validation Plugin -->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<!-- Bootstrap JS con Popper.js incluido -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
<?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('subtitle') ?>
Usa nuestra Calculadora de importes!
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
    <form id="simuladorForm">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="dias">Nº de días
                        <i class="fa fa-info-circle" data-container="body" data-trigger="hover" data-toggle="popover"
                           data-content="Introduce el número de días que vas a trabajar."></i>
                    </label>
                    <input type="text" class="form-control editable" id="dias" name="dias" placeholder="Número de días">

                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="base_imponible">Base Imponible
                        <i class="fa fa-info-circle" data-container="body" data-toggle="popover" data-trigger="hover"
                           data-content="Es el importe bruto que tu proveedor te ha dicho que cobrarás, sin incluir impuestos."></i>
                    </label>
                    <input type="text" class="form-control editable" id="base_imponible"  name="base_imponible" placeholder="Importe bruto">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="iva">IVA
                        <i class="fa fa-info-circle" data-container="body" data-toggle="popover" data-trigger="hover"
                           data-content="El general es 21%. Excepcionalmente se aplica el 4%."></i>
                    </label>
                    <select class="form-control editable" id="iva" name="iva">
                        <option value="4">4%</option>
                        <option value="21" selected>21%</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="irpf">Retención IRPF
                        <i class="fa fa-info-circle" data-container="body" data-toggle="popover" data-trigger="hover"
                           data-content="Excepcionalmente se aplica el 7%. El mínimo es 15%. Recomendamos 19% si tienes 2 o más pagadores."></i>
                    </label>
                    <select class="form-control editable" id="irpf">
                        <option value="7">7%</option>
                        <option value="15" selected>15%</option>
                        <option value="17">17%</option>
                        <option value="19">19%</option>
                        <option value="21">21%</option>
                        <option value="25">25%</option>
                        <option value="30">30%</option>
                    </select>
                </div>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="seguridad_social">Seguridad Social
                    <i class="fa fa-info-circle" data-container="body" data-toggle="popover" data-trigger="hover"
                       data-content="Es proporcional al número de días trabajados."></i>
                </label>
                <input type="text" class="form-control" id="seguridad_social" readonly>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="total">Total Factura
                <i class="fa fa-info-circle" data-container="body" data-toggle="popover" data-trigger="hover"
                       data-content="Coste del espectáculo al Promotor (BI + IVA)."></i>
                </label>
                <input type="text" class="form-control" id="total" readonly>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="cuota_servicio_calculada">Cuota de Servicio Calculada
                <i class="fa fa-info-circle" data-container="body" data-toggle="popover" data-trigger="hover"
                       data-content="Coste de los servicios de Canon. Aplicamos un 5% sobre la BI con independencia del número de días."></i>
                </label>
                <input type="text" class="form-control" id="cuota_servicio_calculada" readonly>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="nomina">Líquido (Nómina)
                <i class="fa fa-info-circle" data-container="body" data-toggle="popover" data-trigger="hover"
                       data-content="Cantidad que recibirás en tu cuenta una vez el Promotor emita la transferencia."></i>
                </label>
                <input type="text" class="form-control" id="nomina" readonly>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
