<?php 
/*******************************************************************************
 * Ejemplo de VISTA que utiliza la PLANTILLA de adminlte 
 * que está en app\Views\plantillas\adminlte.php
 ******************************************************************************/
?>

<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>
<?= $this->section('side_bar') ?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?= $this->include('common/sidebar') ?>
</aside>
<?= $this->endSection() ?>
<?= $this->section('content') ?>
    
<?= $this->endSection() ?>

