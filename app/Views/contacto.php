<?= $this->extend('plantillas/adminlte_navbar_nocard') ?>

<!-- CSS DataTables -->
<?= $this->section('css') ?>
<?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>

<!--  Tïtulo de la página, y la llamaremos en la plantilla -->
<?= $this->section('page_title') ?>
<?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<style>
.logo-container {
    width: 150px;
    height: 150px;
    border-radius: 50%;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 20px; /* Espacio entre el logo y el título */
}

.logo {
    width: 100%;
    height: 100%;
    object-fit: cover;
}
</style>
<section class="content">

    <div class="container">
        <div class="row">
            <!-- Columna para el logo y los datos de contacto -->
            <div class="col-md-4 text-center d-flex align-items-center justify-content-center">
                <div>
                    <div class="logo-container">
                        <img src="assets/images/CANON-oscuro.png" alt="Logo Canon" class="logo">
                    </div>
                    <h2>Canon</h2>
                    <p class="lead mb-5">
                        Calle de la letra musicada, 17 <br>
                        Burjassot 46115<br><br>
                        963 369 639 <br>
                        Lunes a viernes de 9h-15h
                    </p>
                </div>
            </div>
            <!-- Columna para el formulario -->
            <div class="col-md-8">
                <form>
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" class="form-control" id="name" placeholder="Pon aquí tu nombre">
                    </div>
                    <div class="form-group">
                        <label for="email">Correo electrónico</label>
                        <input type="email" class="form-control" id="email" placeholder="Indícanos tu correo electrónico">
                    </div>
                    <div class="form-group">
                        <label for="message">Mensaje</label>
                        <textarea class="form-control" id="message" rows="5" placeholder="Cuéntanos"></textarea>
                    </div>
                    <button type="submit" class="btn btn-info">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- jQuery Validation Plugin -->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<!-- Bootstrap JS con Popper.js incluido -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
<?= $this->endSection() ?>