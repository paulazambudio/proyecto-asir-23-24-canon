<?= $this->extend('plantillas/adminlte_navbar') ?>

<?= $this->section('css')?>
    <?= $this->include('Solicitud/tempus_css') ?>
<?= $this->endSection()?>

<?= $this->section('js')?>
    <?= $this->include('Solicitud/tempus_js') ?>
    <?= $this->include('Solicitud/calculos_js') ?>
<?= $this->endSection()?>

<?= $this->section('title') ?>
Crear factura
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
    <h2><?= $titulo ?></h2>

    <?php if (session()->getFlashdata('errors')): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach (session()->getFlashdata('errors') as $error): ?>
                    <li><?= $error ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?= form_open('factura/store', ['id' => 'formFactura']) ?>
    <div class="form-group">
        <?= form_label('Fecha Emisión:', 'Fecha_Emision') ?>
        <?= form_input('Fecha_Emision', date('Y-m-d'), ['id' => 'Fecha_Emision', 'class' => 'form-control', 'readonly' => 'readonly']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Estado:', 'Estado') ?>
        <?= form_dropdown('Estado', ['Emitida' => 'Emitida', 'Pagada' => 'Pagada', 'Parcialmente Pagada' => 'Parcialmente Pagada', 'Cancelada' => 'Cancelada', 'Rechazada' => 'Rechazada'], 'Emitida', ['id' => 'Estado', 'class' => 'form-control']) ?>

    </div>
    <div class="form-group">
        <?= form_label('Buscar Solicitud:', 'buscador') ?>
        <?= form_input('buscador', '', ['id' => 'buscador', 'class' => 'form-control', 'onkeyup' => 'buscaSugerencia(this.value)']) ?>
        <div id="resultadosBusqueda"></div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Crear Factura</button>
        <a href="<?= previous_url() ?>" class="btn btn-danger">Cancelar</a>
    </div>
    <?= form_close() ?>
</div>

<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
    function buscaSugerencia(texto) {
        fetch('<?= site_url('/factura/buscarSolicitud') ?>', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-Requested-With': 'XMLHttpRequest'
            },
            body: 'texto=' + encodeURIComponent(texto)
        })
        .then(response => response.text())
        .then(html => {
            document.getElementById('resultadosBusqueda').innerHTML = html;
        });
    }
</script>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- jQuery Validation Plugin -->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<!-- Bootstrap JS con Popper.js incluido -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
<!-- Incluir el archivo de validación -->
<script src="<?= base_url('assets/js/validacionfactura_js.js') ?>"></script>
<?= $this->endSection() ?>
