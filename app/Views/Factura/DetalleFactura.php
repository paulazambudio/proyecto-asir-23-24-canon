<?= $this->extend('plantillas/adminlte_navbar_nocard') ?>

<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <?php if (session()->getFlashdata('message')): ?>
                <div class="alert alert-success">
                    <?= session()->getFlashdata('message') ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="invoice p-3 mb-3">
                <!-- Título -->
                <div class="row ">
                    <div class="col-12">
                        <h4 class="text-center">
                            <i class="fas fa-file-invoice"></i> Factura
                            
                        </h4>
                        <h5 class="float-right">Fecha: <?= date('d/m/Y', strtotime($factura->Fecha_Emision)) ?></h5>
                    </div>
                </div>
                <!-- Info de las partes -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        <strong>De:</strong>
                        <address>
                            <strong>Canon</strong><br>
                            Calle de la letra musicada, 17<br>
                            46115 Burjassot (Valencia)<br>
                            <i class="fa-solid fa-phone"></i> 963 369 639<br>
                            <i class="fa-solid fa-envelope"></i> info@canon.com
                        </address>
                    </div>
                    <div class="col-sm-4 invoice-col">
                        <strong>Para:</strong>
                        <address>
                            <strong><?= esc($promotor->Razon_Social) ?> <?= esc($promotor->Nombre) ?> <?= esc($promotor->Apellido1) ?> <?= esc($promotor->Apellido2) ?></strong><br>
                            <?= esc($promotor->Calle) ?>, <?= esc($promotor->Numero) ?><br>
                            <?= esc($promotor->Codigo_Postal) ?>, <?= esc($promotor->Localidad) ?><br>
                            <i class="fa-solid fa-phone"></i> <?= esc($promotor->Telefono) ?><br>
                            <i class="fa-solid fa-envelope"></i> <?= esc($promotor->Email) ?><br>
                        </address>
                    </div>
                    <div class="col-sm-4 invoice-col">
                        <div class="alert alert-danger text-center" style="padding: 5px; margin-bottom: 10px;">
                            <b>ID Factura: <?= esc($factura->Id_Factura) ?></b>
                        </div>
                        <div>
                        <b>Fecha Emisión:</b> <?= esc($factura->Fecha_Emision) ?><br>
                        <b>Estado:</b> <?= esc($factura->Estado) ?>
                        </div>
                        <div class="alert alert-info" style="padding: 5px; margin-bottom: 10px;">
                            <b>Descripción del evento / prestación de servicios:</b>
                            <span><?= esc($solicitud->Descripcion) ?></span>
                        </div>
                    </div>
                </div>



                <!-- Tabla de importes -->
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Concepto</th>
                                    <th>Importe</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Importe Bruto</td>
                                    <td><?= esc($factura->Importe_Bruto) ?> €</td>
                                </tr>
                                <tr>
                                    <td>IVA</td>
                                    <td><?= esc($factura->Importe_IVA) ?> €</td>
                                </tr>
                                
                                <tr >
                                    <td class="text-right"><b>Importe Total</b></td>
                                    <td class="table-success"><b><?= esc($factura->Importe_Total_Factura) ?> €</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- Botones -->
                <div class="row no-print">
                    <div class="col-12">
                        <a href="<?= site_url('/registro') ?>" class="btn btn-primary"><i class="fas fa-arrow-left"></i> Área personal</a>
                        <a href="<?= site_url('/facturas') ?>" class="btn btn-info"><i class="fa-regular fa-folder-open"></i> Mis facturas</a>
                        <button id="btn-print" class="btn btn-secondary float-right"><i class="fas fa-print"></i> Imprimir PDF</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
    document.getElementById('btn-print').addEventListener('click', function () {
        window.print();
    });
</script>
<?= $this->endSection() ?>
