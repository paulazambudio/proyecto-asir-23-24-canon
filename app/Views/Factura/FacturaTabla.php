<?= $this->extend('plantillas/adminlte') ?>

<!-- CSS DataTables -->
<?= $this->section('css') ?>
<?= $this->include('common/datatables') ?>
<?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>

<!-- Título de la página -->
<?= $this->section('page_title') ?>
<?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('side_bar') ?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?= $this->include('common/sidebar') ?>
</aside>
<?= $this->endSection() ?>

<!-- Card -->
<?= $this->section('subtitle') ?>
Tabla de facturas
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="table-responsive">
    <table id="myTable" class="table table-stripped table-bordered" style="width: 100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Fecha Emisión</th>
                <th>Estado</th>
                <th>Importe Bruto</th>
                <th>Importe IVA</th>
                <th>Importe IRPF</th>
                <th>Importe Total</th>
                <th>ID Solicitud</th>
                <th>ID Promotor</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($facturas as $factura): ?>
                <tr>
                    <td><?= esc($factura->Id_Factura) ?></td>
                    <td><?= esc($factura->Fecha_Emision) ?></td>
                    <td><?= esc($factura->Estado) ?></td>
                    <td><?= esc($factura->Importe_Bruto) ?> €</td>
                    <td><?= esc($factura->Importe_IVA) ?> €</td>
                    <td><?= esc($factura->Importe_IRPF) ?> €</td>
                    <td><?= esc($factura->Importe_Total_Factura) ?> €</td>
                    <td><?= esc($factura->Id_Solicitud_Factura) ?></td>
                    <td><?= esc($factura->Id_Promotor_Factura) ?></td>
                    <td class="text-center">
                        <!-- DETALLE -->
                        <a href="<?= site_url('factura/detalle/' . $factura->Id_Factura) ?>" 
                           data-toggle="tooltip" title="Ver Detalles" class="mr-2">
                            Info<span class="fa fa-info-circle text-primary"></span>
                        </a>
                        <!-- Verificación del grupo admin -->
                        <?php if (auth()->user()->inGroup('admin')): ?>
                        <!-- EDICIÓN -->
                        <a href="<?= site_url('factura/editar/' . $factura->Id_Factura) ?>" 
                           data-toggle="tooltip" title="Editar Factura" class="mr-2 text-warning">
                            Editar<span class="fa-solid fa-pencil text-warning"></span>
                        </a>
                        <!-- BORRAR -->
                        <a href="<?= site_url('factura/borrar/' . $factura->Id_Factura) ?>" 
                           onclick="return confirm('¿Estás seguro de que quieres borrar la Factura seleccionada? Esta acción no se puede deshacer')"
                           data-toggle="tooltip" title="Borrar Factura" class="ml-2 text-danger">
                            Borrar<span class="fa fa-trash text-danger"></span>
                        </a>
                        <?php endif; ?>
                        
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->endSection() ?>

<!-- JS DataTables -->
<?= $this->section('js') ?>
<?= $this->include('common/datatables') ?>
<?= $this->include('common/bootstrap') ?>
<script>
    // Inicializar tooltips y DataTables
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('#myTable').DataTable();
    });
</script>
<?= $this->endSection() ?>
