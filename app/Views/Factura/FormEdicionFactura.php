<?= $this->extend('plantillas/adminlte_navbar') ?>

<?= $this->section('title') ?>
<?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="text-center"><?= $titulo ?></h2>
            <?php if (session()->getFlashdata('errors')): ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php foreach (session()->getFlashdata('errors') as $error): ?>
                            <li><?= $error ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>

            <?= form_open('factura/procesarFormularioEdicion/' . $factura->Id_Factura, ['id' => 'formFactura']) ?>
            <div class="form-group">
                <?= form_label('Fecha Emisión:', 'Fecha_Emision') ?>
                <?= form_input('Fecha_Emision', set_value('Fecha_Emision', $factura->Fecha_Emision), ['class' => 'form-control']) ?>
            </div>
            <div class="form-group">
                <?= form_label('Estado:', 'Estado') ?>
              <?= form_dropdown('Estado', [
                    'Emitida' => 'Emitida',
                    'Pagada' => 'Pagada',
                    'Parcialmente Pagada' => 'Parcialmente Pagada',
                    'Cancelada' => 'Cancelada',
                    'Rechazada' => 'Rechazada'
                ], set_value('Estado', $factura->Estado), ['class' => 'form-control']) ?>
            </div>
            <div class="form-group">
                <?= form_label('Importe Bruto:', 'Importe_Bruto') ?>
                <?= form_input('Importe_Bruto', set_value('Importe_Bruto', $factura->Importe_Bruto), ['class' => 'form-control']) ?>
            </div>
            <div class="form-group">
                <?= form_label('Importe IVA:', 'Importe_IVA') ?>
                <?= form_input('Importe_IVA', set_value('Importe_IVA', $factura->Importe_IVA), ['class' => 'form-control']) ?>
            </div>
            <div class="form-group">
                <?= form_label('Importe Total:', 'Importe_Total_Factura') ?>
                <?= form_input('Importe_Total_Factura', set_value('Importe_Total_Factura', $factura->Importe_Total_Factura), ['class' => 'form-control']) ?>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-info">Actualizar</button>
                <a href="<?= site_url('/facturas') ?>" class="btn btn-danger">Cancelar</a>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<?= $this->endSection() ?>
<?= $this->section('js') ?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- jQuery Validation Plugin -->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<!-- Bootstrap JS con Popper.js incluido -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
<!-- Incluir el archivo de validación -->
<script src="<?= base_url('assets/js/validacionfactura_js.js') ?>"></script>
<?= $this->endSection() ?>