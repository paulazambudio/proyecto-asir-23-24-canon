<?php

/**
 * Description of SeleccionTrabajador
 * * @author paula
 */
?>

<?= $this->extend('plantillas/adminlte') ?>

<!-- CSS DataTables -->
<?= $this->section('css') ?>
	<?= $this->include('common/datatables') ?>
	<?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>


<?= $this->section('page_title') ?>
<?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('side_bar') ?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<?= $this->include('common/sidebar') ?>
</aside>
<?= $this->endSection() ?>

<!-- Card -->
<?= $this->section('subtitle') ?>
Selecciona el/los artistas al/los que quieras dar de alta
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<?php if ($this->session->flashdata('error')): ?>
	<p style="color: red;"><?php echo $this->session->flashdata('error'); ?></p>
<?php endif; ?>

<?php if ($this->session->flashdata('exito')): ?>
	<p style="color: green;"><?php echo $this->session->flashdata('exito'); ?></p>
<?php endif; ?>

<form method="post" action="<?php echo site_url('ArtistaController/cargar_artistas'); ?>">
	<label for="seleccion_artistas">Seleccionar Artistas:</label>
	<select id="seleccion_artistas" name="seleccion_artistas">
    	<option value="1">Un Artista</option>
    	<option value="multiples">Múltiples Artistas</option>
	</select>

	<table id="artistasTable" class="table table-stripped table-bordered">
    	<thead>
        	<tr>
            	<th>Seleccionar</th>
            	<th>Nombre</th>
            	<th>NSS</th>
        	</tr>
    	</thead>
    	<tbody>
        	<?php foreach ($artistas as $artista): ?>
            	<tr>
                	<td><input type="checkbox" name="ids_artistas[]" value="<?php echo $artista->Id_Artista; ?>"></td>
                	<td><?php echo $artista->Nombre . ' ' . $artista->Apellido1 . ' ' . $artista->Apellido2; ?></td>
                	<td><?php echo $artista->Nuss; ?></td>
            	</tr>
        	<?php endforeach; ?>
    	</tbody>
	</table>

	<button type="submit" class="btn btn-primary">Cargar Artistas</button>
</form>

<?php if ($this->session->userdata('artistas_seleccionados')): ?>
	<form method="post" action="<?php echo site_url('TrabajadorController/generar_xml'); ?>">
    	<button type="submit" class="btn btn-success mt-3">Generar XML</button>
	</form>
<?php endif; ?>
<?= $this->endSection() ?>

<!-- JS DataTables -->
<?= $this->section('js') ?>
	<?= $this->include('common/datatables') ?>
	<?= $this->include('common/bootstrap') ?>
	<script>
    	$(document).ready(function(){
        	$('#artistasTable').DataTable();
    	});
	</script>
<?= $this->endSection() ?>

