<?php
/**
 * Description of Faq
 *
 * @author paula
 */
?>

<?= $this->extend('plantillas/adminlte_navbar_nocard') ?>

<!-- CSS -->
<?= $this->section('css') ?>
<?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
<?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('subtitle') ?>
Respondemos tus dudas
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="content">
    <div class="row">
        <div class="col-12" id="accordion">
            <div class="card card-primary card-outline">
                <a class="d-block w-100" data-toggle="collapse" href="#collapseOne">
                    <div class="card-header">
                        <h4 class="card-title w-100">
                            1. ¿Cuánto cuesta?
                        </h4>
                    </div>
                </a>
                <div id="collapseOne" class="collapse show" data-parent="#accordion">
                    <div class="card-body">
                        El 5% del importe bruto total, con independencia del número de días que actúes, dónde sea la actuación 
                        y nos lo comuniques de urgencia o con antelación suficiente.
                    </div>
                </div>
            </div>
            <div class="card card-primary card-outline">
                <a class="d-block w-100" data-toggle="collapse" href="#collapseTwo">
                    <div class="card-header">
                        <h4 class="card-title w-100">
                            2. ¿Qué debo poner en el campo "importe bruto" o "base imponible"?
                        </h4>
                    </div>
                </a>
                <div id="collapseTwo" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        Será el importe <strong>sin IVA</strong> que has negociado con el promotor o el mánager, en caso de haberlo. 
                        El importe mínimo legal por actuación es de 150€ pero entendemos que la realidad del mercado es otra, así que aceptamos mínimos de 30€. 
                        Con esta cuantía se cubrirían los costes de todas las partes sin incurrir en cantidades negativas.
                    </div>
                </div>
            </div>
            <div class="card card-primary card-outline">
                <a class="d-block w-100" data-toggle="collapse" href="#collapseThree">
                    <div class="card-header">
                        <h4 class="card-title w-100">
                            3. ¿Por qué no puedo poner el IVA del 10%?
                        </h4>
                    </div>
                </a>
                <div id="collapseThree" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        Siempre aplicamos el IVA del 21%. Cuando Canon interviene, como somos una empresa (persona jurídica) 
                        y no somos una asociación sin ánimo de lucro (tenemos intereses mercantiles) no nos corresponde ninguna modalidad de IVA reducido.
                    </div>
                </div>
            </div>
            <div class="card card-warning card-outline">
                <a class="d-block w-100" data-toggle="collapse" href="#collapseFour">
                    <div class="card-header">
                        <h4 class="card-title w-100">
                            4. Pero entonces... ¿Cuánto cobraré?
                        </h4>
                    </div>
                </a>
                <div id="collapseFour" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        El importe que percibirás está detallado bajo el nombre "Importe líquido" o "Nómina". 
                        Tendrás el ingreso en la cuenta una vez el promotor lo abone a Canon. 
                        Realizamos las transferencias en un plazo de 2 a 4 días hábiles desde que las recibimos. 
                    </div>
                </div>
            </div>
            <div class="card card-warning card-outline">
                <a class="d-block w-100" data-toggle="collapse" href="#collapseFive">
                    <div class="card-header">
                        <h4 class="card-title w-100">
                            5. ¿Por qué os tengo que dar todos estos datos?
                        </h4>
                    </div>
                </a>
                <div id="collapseFive" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        Para poder trabajar 100% online, necesitamos el Número Único de la Seguridad Social y tu número de cuenta, como se lo darías a tu empleador cara a cara... 
                        sólo que ahora rellenas un formulario. Esta información es necesaria para darte de alta en la Seguridad Social y para que recibas tu nómina.
                    </div>
                </div>
            </div>
            <div class="card card-warning card-outline">
                <a class="d-block w-100" data-toggle="collapse" href="#collapseSix">
                    <div class="card-header">
                        <h4 class="card-title w-100">
                            6. ¿Qué pasa si me cancelan la actuación antes de que suceda?
                        </h4>
                    </div>
                </a>
                <div id="collapseSix" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        Primero, que lo sentiremos mucho. Después, es importante que nos lo comuniques <strong>LO ANTES POSIBLE</strong> 
                        para cancelar tu alta en la Seguridad Social enseguida. Si no nos lo dices y se pasa el día de la actuación, <strong>habremos incurrido en gastos</strong>
                        que tendremos que repercutirte, aunque tú no hayas trabajado y no vayas a cobrar.
                    </div>
                </div>
            </div>
            <div class="card card-danger card-outline">
                <a class="d-block w-100" data-toggle="collapse" href="#collapseSeven">
                    <div class="card-header">
                        <h4 class="card-title w-100">
                            7. ¿Quién recibe la factura y quién recibe las altas?
                        </h4>
                    </div>
                </a>
                <div id="collapseSeven" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        Las altas a la Seguridad Social se envían al artista usuario, el día de antes de la actuación. 
                        La factura se envía el día siguiente laborable al promotor pagador de la actuación. 
                        En el caso de que los promotores sean Administraciones públicas, contamos con aplicaciones software específicas.
                    </div>
                </div>
            </div>
            <div class="card card-danger card-outline">
                <a class="d-block w-100" data-toggle="collapse" href="#collapseEight">
                    <div class="card-header">
                        <h4 class="card-title w-100">
                            8. Si tengo otro trabajo por cuenta ajena en otra empresa, ¿puedo facturar con vosotros?
                        </h4>
                    </div>
                </a>
                <div id="collapseEight" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        Sí, no hay ninguna incompatibilidad legal ni fiscal. 
                        Las retribuciones y cotizaciones de Canon te saldrán también reflejadas automáticamente en el borrador de la declaración de la Renta. 
                    </div>
                </div>
            </div>
            <div class="card card-danger card-outline">
                <a class="d-block w-100" data-toggle="collapse" href="#collapseNine">
                    <div class="card-header">
                        <h4 class="card-title w-100">
                            9. Estoy cobrando el paro y me ha salido esta actuación. ¿Gestionáis certificado de empresa?
                        </h4>
                    </div>
                </a>
                <div id="collapseNine" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        Sí, y aunque no estuvieras cobrándolo también: para todos los artistas y para todas las actuaciones, 
                        Canon comunica telemáticamente el certificado de empresa al SEPE. 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mt-3 text-center">
            <p class="lead">
                Si no encuentras la respuesta que buscas o tienes más preguntas, <br>
                <a href="<?= base_url('/contacto'); ?>">contáctanos</a>.
            </p>
        </div>
    </div>
</section>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- Bootstrap JS con Popper.js incluido -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
<?= $this->endSection() ?>