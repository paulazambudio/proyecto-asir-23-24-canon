<!-- footer -->
<!--  así los div se distribuyan uniformemente en una línea -->
<footer class="no-print" style="display: flex; justify-content: space-between; align-items: center;">
    <!-- con esta clase los divs se ocultan en 
    pantallas pequeñas y se muestran en medianas y grandes-->
    <div class="d-none d-sm-block">
        Proyecto ASIR  - Paula ZQ
    </div>
    <div class="d-none d-sm-block">
        © 
        <span id="year"></span> 
        - 
        <strong style="color: #10adad;">DeegiWeb Studio</strong>.
        Powered by <strong><a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    </div>
    <div class="d-none d-sm-block">
        <b>Version</b> 3.2.0
    </div>
</footer>

<!-- script para obtener el año actual -->
<script>
    document.getElementById('year').textContent = new Date().getFullYear();
</script>
