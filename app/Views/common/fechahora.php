<?php
/*
 * Conjunto de recursos necesarios para GetDatePicker.
 * Simplificación de su llamada en plantilla
 */
?>

<?= $this->section('fechahora_css') ?>
    <link href="<?= base_url('assets/css/tempusdominus-bootstrap-4.min.css') ?>" rel="stylesheet">
<?= $this->endSection() ?>

<?= $this->section('fechahora_js') ?>    
<script src="<?= base_url('assets/js/moment.min.js') ?>"></script>
<script src="<?= base_url('assets/js/moment-with-locales.min.js') ?>"></script>
<script src="<?= base_url('assets/js/tempusdominus-bootstrap-4.min.js') ?>"></script>
<script>
    $(document).ready(function () {
        $('.datetimepicker-demo').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY, HH:mm', //L para sólo fecha y LT sólo hora
            useCurrent: false,
            icons: {
                time: "fa fa-clock"
            },
            daysOfWeekDisabled: [0, 6]
        });
    });
</script>
<?= $this->endSection() ?>