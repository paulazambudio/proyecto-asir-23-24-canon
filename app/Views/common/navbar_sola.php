<!-- navbar -->

<ul class="navbar-nav no-print">
    <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= site_url('/')?>" class="nav-link">Inicio</a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">

        <a href="<?= site_url('/contacto')?>" class="nav-link">Contacto</a>
    </li>
</ul>

<!-- barra de búsqueda -->
<ul class="navbar-nav ml-auto no-print">

<!-- perfil de usuario, pantalla completa -->
    <li class="nav-item">
        <a class="nav-link" href="<?= site_url('login')?>">
            <i class="fa-solid fa-user"></i>
        </a>
    </li>
<!-- pantalla completa -->        
    <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
        </a>
    </li>
</ul>

