<?php
/*
 * Conjunto de recursos necesarios para DataTables.
 * Simplificación de su llamada en plantilla
 */
?>

<?= $this->section('css') ?>
<link href="<?= base_url('assets/css/dataTables.bootstrap4.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('assets/css/datatables.responsive.bootstrap4.css') ?>" rel="stylesheet">
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/js/dataTables.bootstrap4.min.js') ?>"></script>
<!-- responsividad -->
<script src="<?= base_url('assets/js/dataTables.js') ?>"></script>
<script src="<?= base_url('assets/js/dataTables.bootstrap4.js') ?>"></script>
<script src="<?= base_url('assets/js/dataTables.responsive.js') ?>"></script>
<script src="<?= base_url('assets/js/responsive.bootstrap4.js') ?>"></script>
<script>
    //una vez el DOM esté listo
    $(document).ready(function () {
        //destruimos DataTables
        $('#myTable').DataTable().destroy();

        // y re-inicializamos DataTables con el idioma español
        //descargado de su web oficial y subido a la carpeta local public
        $('#myTable').DataTable({
            language: {
                url: '/languages/es-ES.json'
            }
        });
    });
</script>
<?= $this->endSection() ?>
