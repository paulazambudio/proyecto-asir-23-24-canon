<!-- Sidebar -->

<a href="<?= base_url('/'); ?>" class="brand-link">
    <img src="assets/images/Canon-logo2.png" alt="Canon Logo" class="brand-image img-circle elevation-3">
    <span class="brand-text font-weight-light">Canon</span>
</a>

<div class="sidebar no-print">
    <!-- área del usuario -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <?php if (auth()->loggedIn()): ?>
        <div class="image">
            <img src="assets/images/userlogo.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <?php endif; ?>
        <div class="info">
            <?php if (auth()->loggedIn()): ?>
            <a href="/logout" class="d-block"><?= auth()->user()->username; ?> &nbsp; <i class="fa-solid fa-arrow-right-from-bracket"></i></a>
            <?php else: ?>
                <a class="d-block">Invitad@ (web pública)</a>
            <?php endif; ?>
        </div>
    </div>

    <!-- inicio de los iconos-->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-header">PÚBLICA</li>
            <li class="nav-item">
                <a href="/simulador" class="nav-link">
                    <i class="nav-icon fa-solid fa-calculator"></i>
                    <p>
                        Simulador
                        <span class="badge badge-danger right">¡Nuevo!</span>
                    </p>
                </a>
            </li>
            <!-- <li class="nav-item">
                <a href="./registro" class="nav-link">
                    <i class="nav-icon fa-solid fa-file-contract"></i>
                    <p>
                        Regístrate
                    </p>
                </a>
            </li>-->
            <li class="nav-item">
                <a href="./login" class="nav-link">
                    <i class="nav-icon fas fa-arrow-right-to-bracket"></i>
                    <p>
                        Login
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="quienes_somos" class="nav-link">
                    <i class="nav-icon fa-solid fa-circle-info"></i>
                    <p>
                        Quiénes somos
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="./preguntas_frecuentes" class="nav-link">
                    <i class="nav-icon fa-solid fa-question"></i>
                    <p>
                        Preguntas frecuentes
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="./contacto" class="nav-link">
                    <i class="nav-icon fa-solid fa-envelope"></i>
                    <p>
                        Contacto
                    </p>
                </a>
            </li>
            
            <!-- fin parte pública -->
            <!-- inicio parte privada - para USUARIOS LOGGEADOS -->
            <?php if (auth()->loggedIn()): ?>
                <li class="nav-header"> ZONA USUARIOS </li>
                
                <li class="nav-item">
                    <a href="/solicitudes" class="nav-link">
                        <i class="nav-icon fa-regular fa-folder-open"></i>
                        <p>
                            Mis solicitudes
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/facturas" class="nav-link">
                        <i class="nav-icon fa-solid fa-file-invoice"></i>
                        <p>
                            Mis facturas
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Calendario
                            <span class="badge badge-warning right">2 <i class="fa-regular fa-bell"></i> </span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa-solid fa-music"></i>
                        <p>
                            Mis grupos
                            <span class="badge badge-warning right"><i class="fa-solid fa-person-digging"></i></span>
                        </p>
                    </a>
                </li>
            <?php endif; ?>
            <!-- parte privada - para USUARIOS LOGGEADOS que son ADMIN-->
            <!-- se añade auth()->user()->inGroup('admin'))-->
            <?php if (auth()->loggedIn() AND  auth()->user()->inGroup('admin')): ?>
                <li class="nav-header"> ZONA ADMIN </li>
                <li class="nav-item">
                    <a href="/auth/user" class="nav-link">
                        <i class="nav-icon fa-solid fa-user"></i>
                        <p>
                            Listado de Usuarios
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/factura/crear" class="nav-link">
                        <i class="nav-icon fa-solid fa-file-import"></i>
                        <p>
                            Generar factura
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/artistas" class="nav-link">
                        <i class="nav-icon fa-regular fa-circle-user"></i>
                        <p>
                            Todos los artistas
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/promotores" class="nav-link">
                        <i class="nav-icon fa-regular fa-address-card"></i>
                        <p>
                            Todos los promotores
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/solicitudes" class="nav-link">
                        <i class="nav-icon fa-regular fa-folder-open"></i>
                        <p>
                            Todas las solicitudes
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/facturas" class="nav-link">
                        <i class="nav-icon fa-solid fa-file-invoice"></i>
                        <p>
                            Todas las facturas
                        </p>
                    </a>
                </li>
            <?php endif; ?>
                
        </ul>
    </nav>
</div>
