<!-- navbar -->

<ul class="navbar-nav no-print">
    <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= site_url('/')?>" class="nav-link">Inicio</a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= site_url('/contacto')?>" class="nav-link">Contacto</a>
    </li>
</ul>


<ul class="navbar-nav ml-auto no-print">

<!-- SEGÚN SI ESTÁ LOGGEADO O NO: iniciar sesión / salir -->
    <?php if (auth()->loggedIn()): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?= site_url('logout') ?>">
                Logout
                <i class="fa-solid fa-arrow-right-from-bracket"></i>
                
            </a>
        </li>
    <?php else: ?>
        <li class="nav-item">
            <a class="nav-link" href="<?= site_url('login') ?>">
                Login
                <i class="fa-solid fa-right-to-bracket"></i>
                
            </a>
        </li>
    <?php endif; ?>
    
<!-- pantalla completa -->    
    <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
        </a>
    </li>
</ul>

