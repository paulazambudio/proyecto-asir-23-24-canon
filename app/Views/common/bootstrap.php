<?php
/*
 * Unificación de recursos de Bootstrap CDN
 */
?>
<?= $this->section('bootstrap_css') ?>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" rel="stylesheet">
<?= $this->endSection() ?>

<?= $this->section('bootstrap_js') ?>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"></script>
<?= $this->endSection() ?>