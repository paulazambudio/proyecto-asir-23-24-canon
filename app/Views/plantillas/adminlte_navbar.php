<!DOCTYPE html>
<html lang="es" style="height: auto;">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php 
        // Obtener la cadena de la URL
        $cadena_uri_actual = uri_string();
        
        // Reemplazar los delimitadores específicos por espacios
        $cadena_procesada = str_replace(['_', '/', '-'], ' ', $cadena_uri_actual);
        
        // Dividir la cadena en palabras
        $palabras_uri = explode(' ', $cadena_procesada);
        
        // Convertir la primera letra de la primera palabra a mayúscula y las demás palabras a minúscula
        foreach ($palabras_uri as $indice => $palabra) {
            if ($indice == 0) {
                $palabras_uri[$indice] = ucfirst(strtolower($palabra)); // Primera palabra en mayúscula
            } else {
                $palabras_uri[$indice] = strtolower($palabra); // Las demás palabras en minúscula
            }
        }
        // Reconstruir la cadena
        $titulo_formateado = implode(' ', $palabras_uri);
        ?>
        <title> Canon | <?= esc($titulo_formateado); ?></title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
        <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome/all.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/css/adminlte.min.css') ?>">

        <!-- CSS ocasional -->
        <?= $this->renderSection('css') ?>
    </head>    
    <!-- hold-transition layout-top-nav sirve para una plantilla sin barra lateral -->
    <body class="hold-transition layout-top-nav" style="height: auto;">

        <div class="wrapper">

            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <?= $this->include('common/navbar') ?>
            </nav>

            <?= $this->renderSection('side_bar') ?>


            <div class="content-wrapper" style="min-height: 1604.8px;">

                <section class="content-header">
                    <div class="container-fluid">
                        <h1><?= $this->renderSection('page_title') ?></h1>
                    </div>
                </section>

                <section class="content">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?= $this->renderSection('subtitle') ?></h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <?= $this->renderSection('content') ?>
                        </div>



                    </div>
                </section>

            </div>

            <footer class="main-footer">
                <?= $this->include('common/footer') ?>
            </footer>

            <!-- js AdminLTE-->
            <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/adminlte.min.js') ?>"></script>

            <!-- JS ocasional -->
            <?= $this->renderSection('js') ?>
    </body>
</html>

