<?= $this->extend('plantillas/adminlte_navbar_nocard') ?>

<?= $this->section('title') ?>
<?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="text-center"><?= $titulo ?></h2>
            <?php if (session()->getFlashdata('message')): ?>
                <div class="alert alert-success">
                    <?= session()->getFlashdata('message') ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="invoice p-3 mb-3">
                <!-- Title row -->
                <div class="row">
                    <div class="col-12">
                        <h4>
                            <i class="fas fa-music"></i> Canon
                            <small class="float-right">Fecha: <?= date('d/m/Y') ?></small>
                        </h4>
                    </div>
                </div>
                <!-- Info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        De
                        <address>
                            <strong>Canon</strong><br>
                            Calle de la letra musicada, 17 <br>
                            46115 Burjassot (Valencia)<br>
                            <i class="fa-solid fa-phone"></i> 963 369 639<br>
                            <i class="fa-solid fa-envelope"></i> info@canon.com
                        </address>
                    </div>
                    <div class="col-sm-4 invoice-col">
                        Para
                        <address>
                            <strong><?= esc($promotor->Razon_Social) ?><?= esc($promotor->Nombre) ?> <?= esc($promotor->Apellido1) ?> <?= esc($promotor->Apellido2) ?></strong><br>
                            <?= esc($promotor->Calle) ?>, <?= esc($promotor->Numero) ?><br>
                            <?= esc($promotor->Codigo_Postal) ?>, <?= esc($promotor->Localidad) ?><br>
                            <i class="fa-solid fa-phone"></i> <?= esc($promotor->Telefono) ?><br>
                            <i class="fa-solid fa-envelope"></i> <?= esc($promotor->Email) ?><br>
                            
                        </address>
                    </div>
                    <div class="col-sm-4 invoice-col">
                        <b class="alert alert-danger text-center">ID Solicitud: <?= esc($solicitud->Id_Solicitud) ?></b><br>
                        <br>
                        <strong>Ubicación:</strong> <?= esc($solicitud->Ubicacion_Evento) ?><br>
                        <b>Fecha de Solicitud:</b> <?= esc($solicitud->Fecha_Solicitud) ?><br>
                        <b>Fecha Inicio (Seguridad Social):</b> <?= esc($solicitud->Fecha_Inicio) ?><br>
                        <b>Fecha Fin (Seguridad Social):</b> <?= esc($solicitud->Fecha_Fin) ?><br>
                    </div>
                </div>

                <!-- Advertencia -->
                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-warning text-center" role="alert">
                            <strong>Advertencia:</strong> Esta solicitud es un presupuesto y no una factura definitiva.
                        </div>
                    </div>
                </div>

                <!-- Table row -->
                <div class="row table-responsive text-center">
                    <div class="col-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Descripción del evento</th>
                                    <th>Importe bruto</th>
                                    <th>21% IVA </th>
                                    <th>IRPF escogido</th>
                                    <th>Cuota de Servicio</th>
                                    <th>Nómina a percibir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= esc($solicitud->Descripcion) ?></td>
                                    <td class="table-info"><?= isset($solicitud->Importe_Bruto) ? esc($solicitud->Importe_Bruto) . ' €' : 'N/A' ?></td>
                                    <td><?= isset($solicitud->Importe_IVA) ? esc($solicitud->Importe_IVA) . ' €' : 'N/A' ?></td>
                                    <td><?= isset($solicitud->Importe_IRPF) ? esc($solicitud->Importe_IRPF) . ' €' : 'N/A' ?></td>
                                    <td><?= isset($solicitud->Cuota_Servicio) ? esc($solicitud->Cuota_Servicio) . ' €' : 'N/A' ?></td>
                                    <td class="table-success"><?= isset($solicitud->Importe_Nomina) ? esc($solicitud->Importe_Nomina) . ' €' : 'N/A' ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <!-- Button row -->
                <div class="row no-print">
                    <div class="col-12">
                        <a href="<?= site_url('/registro') ?>" class="btn btn-primary"><i class="fas fa-arrow-left"></i> Área personal</a>
                        <a href="<?= site_url('/solicitudes') ?>" class="btn btn-info"><i class="fa-regular fa-folder-open"></i> Mis solicitudes</a>
                        <button id="btn-print" class="btn btn-secondary float-right"><i class="fas fa-print"></i> Imprimir PDF</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('js') ?>

<script>
    document.getElementById('btn-print').addEventListener('click', function () {
        window.print();
    });

</script>
<?= $this->endSection() ?>