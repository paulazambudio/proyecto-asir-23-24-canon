<?php
/**
 * Description of FormEdicionSolicitud
 *
 * @author paula
 */ ?>

<?= $this->extend('plantillas/adminlte_navbar') ?>

<?= $this->section('title') ?>
<?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
    <h2><?= $titulo ?></h2>

    <?php if (isset($errors)): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errors as $error): ?>
                    <li><?= $error ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?= form_open('solicitud/editar/' . $solicitud->Id_Solicitud) ?>
    <div class="form-group">
        <?= form_label('Fecha Solicitud:', 'Fecha_Solicitud') ?>
        <?= form_input('Fecha_Solicitud', set_value('Fecha_Solicitud', $solicitud->Fecha_Solicitud), ['id' => 'Fecha_Solicitud', 'class' => 'form-control', 'readonly' => 'readonly']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Fecha Inicio:', 'Fecha_Inicio') ?>
        <?= form_input('Fecha_Inicio', set_value('Fecha_Inicio', $solicitud->Fecha_Inicio), ['id' => 'Fecha_Inicio', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Fecha Fin:', 'Fecha_Fin') ?>
        <?= form_input('Fecha_Fin', set_value('Fecha_Fin', $solicitud->Fecha_Fin), ['id' => 'Fecha_Fin', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Ubicación Evento:', 'Ubicacion_Evento') ?>
        <?= form_input('Ubicacion_Evento', set_value('Ubicacion_Evento', $solicitud->Ubicacion_Evento), ['id' => 'Ubicacion_Evento', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Descripción:', 'Descripcion') ?>
        <?= form_textarea('Descripcion', set_value('Descripcion', $solicitud->Descripcion), ['id' => 'Descripcion', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Importe Bruto:', 'Importe_Bruto') ?>
        <?= form_input('Importe_Bruto', set_value('Importe_Bruto', $solicitud->Importe_Bruto), ['id' => 'Importe_Bruto', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Porcentaje IRPF:', 'Porcentaje_IRPF') ?>
        <?= form_dropdown('Porcentaje_IRPF', ['2' => '2%', '7' => '7%', '15' => '15%', '19' => '19%', '21' => '21%', '25' => '25%', '30' => '30%'], set_value('Porcentaje_IRPF', $solicitud->Porcentaje_IRPF), ['id' => 'Porcentaje_IRPF', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Guardar Cambios</button>
        <a href="<?= site_url('/solicitudes') ?>" class="btn btn-danger">Cancelar</a>
    </div>
    <?= form_close() ?>
</div>
<?= $this->endSection() ?>
