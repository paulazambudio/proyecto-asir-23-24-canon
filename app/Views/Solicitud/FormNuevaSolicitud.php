<?= $this->extend('plantillas/adminlte_navbar') ?>

<?= $this->section('css')?>
    <?php echo $this->include('Solicitud/tempus_css') ?>
<?= $this->endSection()?>

<?= $this->section('js')?>
    <?= $this->include('Solicitud/tempus_js') ?>
 <?= $this->include('Solicitud/calculos_js') ?>
<?= $this->endSection()?>


<?= $this->section('title') ?>
<?= $titulo ?>
<?= $this->endSection() ?>



<?= $this->section('content') ?>
<style>
    .error {
        color: red;
        font-weight: normal;
    }
    label.error {
        color: red;
        font-weight: normal;
        margin-top: 5px;
    }
    input.error, select.error, textarea.error {
        border-color: red;
        background-color: #ffffcc;
    }
</style>


<div class="container">
    <h2><?= $titulo ?></h2>

    <?php if (session()->getFlashdata('errors')): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach (session()->getFlashdata('errors') as $error): ?>
                    <li><?= $error ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?= form_open('nueva_solicitud/procesar', ['id' => 'formSolicitud']) ?>
    <div class="form-group">
        <?= form_label('Fecha Solicitud:', 'Fecha_Solicitud') ?>
        <?= form_input('Fecha_Solicitud', date('Y-m-d H:i:s'), ['id' => 'Fecha_Solicitud', 'class' => 'form-control', 'readonly' => 'readonly']) ?>
    </div>
    <div class='row'>
        <div class='col-md-5'>
            <div class="form-group">
                <?= form_label('Fecha Inicio:', 'Fecha_Inicio') ?>
                <div class="input-group date" id="datetimepicker7" data-target-input="nearest">
                    <?= form_input('Fecha_Inicio', set_value('Fecha_Inicio', ''), ['id' => 'Fecha_Inicio', 'class' => 'form-control datetimepicker-input', 'data-target' => '#datetimepicker7', 'required' => 'required']) ?>
                    <div class="input-group-append" data-target="#datetimepicker7" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class='col-md-5'>
            <div class="form-group">
                <?= form_label('Fecha Fin:', 'Fecha_Fin') ?>
                <div class="input-group date" id="datetimepicker8" data-target-input="nearest">
                    <?= form_input('Fecha_Fin', set_value('Fecha_Fin', ''), ['id' => 'Fecha_Fin', 'class' => 'form-control datetimepicker-input', 'data-target' => '#datetimepicker8', 'required' => 'required']) ?>
                    <div class="input-group-append" data-target="#datetimepicker8" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('Ubicación Evento:', 'Ubicacion_Evento') ?>
        <?= form_input('Ubicacion_Evento', set_value('Ubicacion_Evento', ''), ['id' => 'Ubicacion_Evento', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Descripción:', 'Descripcion') ?>
        <?= form_textarea('Descripcion', set_value('Descripcion', ''), ['id' => 'Descripcion', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Importe Bruto:', 'Importe_Bruto') ?>
        <?= form_input('Importe_Bruto', set_value('Importe_Bruto', ''), ['id' => 'Importe_Bruto', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Porcentaje IVA:', 'Porcentaje_IVA') ?>
        <?= form_input('Porcentaje_IVA', '21', ['id' => 'Porcentaje_IVA', 'class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Importe IVA:', 'Importe_IVA') ?>
        <?= form_input('Importe_IVA', set_value('Importe_IVA', ''), ['id' => 'Importe_IVA', 'class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Porcentaje IRPF:', 'Porcentaje_IRPF') ?>
        <?= form_dropdown('Porcentaje_IRPF', ['2' => '2%', '7' => '7%', '15' => '15%', '19' => '19%', '21' => '21%', '25' => '25%', '30' => '30%'], '15', ['id' => 'Porcentaje_IRPF', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Importe IRPF:', 'Importe_IRPF') ?>
        <?= form_input('Importe_IRPF', set_value('Importe_IRPF', ''), ['id' => 'Importe_IRPF', 'class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Importe Nómina:', 'Importe_Nomina') ?>
        <?= form_input('Importe_Nomina', set_value('Importe_Nomina', ''), ['id' => 'Importe_Nomina', 'class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Cuota Servicio:', 'Cuota_Servicio') ?>
        <?= form_input('Cuota_Servicio', set_value('Cuota_Servicio', ''), ['id' => 'Cuota_Servicio', 'class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Promotor:', 'buscador') ?>
        <?= form_input('buscador', '', ['id' => 'buscador', 'class' => 'form-control', 'onkeyup' => 'buscaSugerencia(this.value)']) ?>
        <!-- en span id="textoBuscar" se pintan las sugerencias de promotor que coinciden con el texto buscado -->
	<p>Promotor: <span id="textoBuscar"></span></p>
    </div>
   
    <div class="form-group">
        <button type="submit" class="btn btn-info">Guardar</button>
        <a href="<?= previous_url() ?>" class="btn btn-danger">Cancelar</a>
    </div>
    <?= form_close() ?>
</div>
<?= $this->endSection() ?>
