<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * Vamos a ver como funciona Tempus-Dominus
 ******************************************************************************/
?>

<?= $this->extend('templates/page') ?>

<?= $this->section('css')?>
    <?php echo $this->include('tempus/tempus_css') ?>
<?= $this->endSection()?>

<?= $this->section('js')?>
    <?= $this->include('tempus/tempus_js') ?>
<?= $this->endSection()?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
    <?= form_open('/tempus/form') ?>
    <!-- Controles enlazados: la fecha del primero es el límite inferior del segundo -->
    <h3>Controles enlazados</h3>
    <div class='col-md-5'>
        <div class="form-group">
           <div class="input-group date" id="datetimepicker7" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker7"/>
                <div class="input-group-append" data-target="#datetimepicker7" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class='col-md-5'>
        <div class="form-group">
           <div class="input-group date" id="datetimepicker8" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker8"/>
                <div class="input-group-append" data-target="#datetimepicker8" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
    </div>
    
    
    <?= form_submit('Enviar','enviar',['class'=>'btn btn-primary']) ?>
    <?= form_close() ?>
</div>
<?= $this->endSection() ?>
