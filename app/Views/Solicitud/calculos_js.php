<script>
    $(function () {
        // Event listeners para calcular valores conforme se insertan (cuando se modifica el campo de inserción)
        $('#Importe_Bruto, #Porcentaje_IRPF, #Fecha_Inicio, #Fecha_Fin').on('input change', calculateValues);

        function calculateValues() {
            let importeBruto = parseFloat($('#Importe_Bruto').val()) || 0;
            let porcentajeIVA = 21;
            let porcentajeIRPF = parseFloat($('#Porcentaje_IRPF').val()) || 0;
            let cuotaServicio = (importeBruto * 0.05).toFixed(2);
            let importeIVA = (importeBruto * (porcentajeIVA / 100)).toFixed(2);
            let importeIRPF = (importeBruto * (porcentajeIRPF / 100)).toFixed(2);

            // Calcular días de alta
            let fechaInicio = $('#Fecha_Inicio').val();
            let fechaFin = $('#Fecha_Fin').val();
            let diasAlta = 1;
            if (fechaInicio && fechaFin) {
                let startDate = moment(fechaInicio, "DD/MM/YYYY");
                let endDate = moment(fechaFin, "DD/MM/YYYY");
                diasAlta = endDate.diff(startDate, 'days') + 1;
            }

            let costeSeguridadSocial = (importeBruto * 0.29 * diasAlta).toFixed(2);
            let importeNomina = (importeBruto - importeIRPF - cuotaServicio - costeSeguridadSocial).toFixed(2);

            $('#Importe_IVA').val(importeIVA);
            $('#Cuota_Servicio').val(cuotaServicio);
            $('#Importe_IRPF').val(importeIRPF);
            $('#Importe_Nomina').val(importeNomina);
        }
    });
 </script>