<!-- Moment.js -->
<script src="<?=base_url('assets/js/moment/moment.js')?>" ></script>
<!-- Para trabajar con locale -->
<script src="<?=base_url('assets/js/moment/moment-with-locales.js')?>"></script>
<!-- El locale es del español -->
<script src="<?=base_url('assets/js/moment/locale/es.js')?>"></script>

<!-- Tempus-Dominus v5.39 -->
<script src="<?=base_url('assets/js/tempus-dominus/tempusdominus-bootstrap-4.js')?>" ></script>

<!-- jQuery Validation Plugin -->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>

<script>
    function buscaSugerencia(texto) {
        	$.ajax({
                    url: "<?= site_url('buscar')?>",
                    method:  'POST',
                    dataType: 'html',
                    data:{"texto": texto},
                    headers: {'X-Requested-With': 'XMLHttpRequest'},
                    success: function(data) {
                            // Update the content on success
                            $("#textoBuscar").html(data);
                            },
                    error: function(error) {
                    // Handle errors
                            console.error('Error:', error);
                            }
        	});
    	}
    //inicialización tras la carga del DOM
    $(function () {
        // Obtener la fecha actual con una nueva instancia de Date, que contiene fecha y hora actuales
        let today = new Date();
        //Primer selector
        $('#datetimepicker7').datetimepicker({
            locale: 'es', //localización en español
            format: 'L', //formato de fecha local
            minDate: today // fecha mínima disponible el día actual
        });
        //Segundo Selector
        $('#datetimepicker8').datetimepicker({
            locale: 'es',
            useCurrent: false, // Impide que el segundo selector use la fecha y hora actual como valor inicial.
            format: 'L',
            minDate: today
        });
        // Configurar un evento que se activa cuando cambia la fecha en #datetimepicker7 (Primer selector)
        $("#datetimepicker7").on("change.datetimepicker", function (e) {
            $('#datetimepicker8').datetimepicker('minDate', e.date);// Establece la fecha mínima en el SegundoSelector basada en la fecha seleccionada en PrimerSelector.
        });
        // Configurar un evento que se activa cuando cambia la fecha en #datetimepicker8 (Segundo selector)
        $("#datetimepicker8").on("change.datetimepicker", function (e) {
            $('#datetimepicker7').datetimepicker('maxDate', e.date); // Establece la fecha máxima en #datetimepicker7 basada en la fecha seleccionada en #datetimepicker8.
        });
        
        // Configuración de la validación del formulario
        $("#formSolicitud").validate({
            rules: {
                Fecha_Solicitud: {
                    required: true,
                    date: true
                },
                Fecha_Inicio: {
                    required: true,
                    //date: true
                },
                Fecha_Fin: {
                    required: true,
                    //date: true
                },
                Ubicacion_Evento: {
                    maxlength: 255
                },
                Descripcion: {
                    maxlength: 65535
                },
                Importe_Bruto: {
                    required: true,
                    number: true,
                    min: 30
                },
                Porcentaje_IRPF: {
                    required: true,
                    number: true
                },
                CIF: {
                    required: true,
                    minlength: 9,
                    maxlength: 9
                }
            },
            messages: {
                Fecha_Solicitud: {
                    required: "Por favor, introduce la fecha de solicitud.",
                    date: "Por favor, introduce una fecha válida."
                },
                Fecha_Inicio: {
                    required: "Por favor, introduce la fecha de inicio.",
                    date: "Por favor, introduce una fecha válida."
                },
                Fecha_Fin: {
                    required: "Por favor, introduce la fecha de fin.",
                    date: "Por favor, introduce una fecha válida."
                },
                Importe_Bruto: {
                    required: "Por favor, introduce el importe bruto.",
                    number: "Por favor, introduce un número válido.",
                    min: "El importe bruto debe ser al menos {0}€."
                },
                Porcentaje_IRPF: {
                    required: "Por favor, selecciona el porcentaje de IRPF.",
                    number: "Por favor, selecciona un número válido."
                },
                CIF: {
                    required: "Por favor, introduce el CIF del promotor.",
                    minlength: "El CIF debe tener al menos {0} caracteres.",
                    maxlength: "El CIF no puede tener más de {0} caracteres."
                }
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            onfocusout: function (element) {
                $(element).valid();
            },
            onkeyup: function (element) {
                $(element).valid();
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
        
        
        
    });
 </script>
        
