<!-- Tempus Dominus Styles -->
<link rel="stylesheet" href="<?=base_url('assets/css/tempusdominus-bootstrap-4.min.css')?>" >

<style>
    .error {
        color: red;
        font-weight: normal;
    }
    label.error {
        color: red;
        font-weight: normal;
        margin-top: 5px;
    }
    input.error, select.error, textarea.error {
        border-color: red;
        background-color: #ffffcc;
    }
</style>