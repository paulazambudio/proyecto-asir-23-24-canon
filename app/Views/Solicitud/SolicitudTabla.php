<?php
/**
 * Description of SolicitudTabla
 *
 * @author paula
 */
?>

<!-- Extendemos la plantilla deseada -->
<?= $this->extend('plantillas/adminlte') ?>

<!-- CSS DataTables -->
<?= $this->section('css') ?>
<?= $this->include('common/datatables') ?>
<?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>

<!--  Título de la página, y la llamaremos en la plantilla -->
<?= $this->section('page_title') ?>
<?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('side_bar') ?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?= $this->include('common/sidebar') ?>
</aside>
<?= $this->endSection() ?>

<!-- Card -->
<?= $this->section('subtitle') ?>
Tabla de solicitudes
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="table-responsive">
    <table id="myTable" class="table table-stripped table-bordered" style="width: 100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Fecha Solicitud</th>
                <th>Fecha Inicio Alta Seguridad Social</th>
                <th>Fecha Fin Alta Seguridad Social</th>
                <th>Ubicación del evento</th>
                <th>Descripción</th>
                <th>Importe bruto / Base imponible</th>
                <th>Importe IVA</th>
                <th>Importe IRPF</th>
                <th>Importe Nómina</th>
                <th>Cuota Servicio</th>
                <th>ID Artista</th>
                <th>ID Promotor</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($solicitudes as $solicitud): ?>
                <tr>
                    <td><?= $solicitud->Id_Solicitud ?></td>
                    <td><?= $solicitud->Fecha_Solicitud ?></td>
                    <td><?= $solicitud->Fecha_Inicio ?></td>
                    <td><?= $solicitud->Fecha_Fin ?></td>
                    <td><?= $solicitud->Ubicacion_Evento ?></td>
                    <td><?= $solicitud->Descripcion ?></td>
                    <td><?= $solicitud->Importe_Bruto ?></td>
                    <td><?= $solicitud->Importe_IVA ?></td>
                    <td><?= $solicitud->Importe_IRPF ?></td>
                    <td><?= $solicitud->Importe_Nomina ?></td>
                    <td><?= $solicitud->Cuota_Servicio ?></td>
                    <td><?= $solicitud->Id_Artista_Solicitud ?></td>
                    <td><?= $solicitud->Id_Promotor_Solicitud ?></td>
                    <td class="text-center">
                        <!-- DETALLE -->
                        <a href="<?= site_url('nueva_solicitud/detalle/' . $solicitud->Id_Solicitud) ?>" 
                           data-toggle="tooltip" title="Ver Detalles" class="mr-2">
                            Info<span class="fa fa-info-circle text-primary">
                            </span>
                        </a>
                        <!-- EDICIÓN -->
                        <a href="<?= site_url('solicitud/editar/' . $solicitud->Id_Solicitud) ?>" 
                           data-toggle="tooltip" title="Editar Solicitud" class="mr-2 text-warning">
                            Editar<span class="fa-solid fa-pencil text-warning">
                            </span>
                        </a>
                        <!-- BORRAR -->
                        <a href="<?= site_url('solicitud/borrar/' . $solicitud->Id_Solicitud) ?>" 
                           onclick="return confirm('¿Estás seguro de que quieres borrar la Solicitud seleccionada? Esta acción no se puede deshacer')"
                           data-toggle="tooltip" title="Borrar Solicitud" class="ml-2 text-danger">
                            Borrar<span class="fa fa-trash text-danger">
                            </span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->endSection() ?>

<!-- JS DataTables -->
<?= $this->section('js') ?>
    <?= $this->include('common/datatables') ?>
    <?= $this->include('common/bootstrap') ?>
    <script>
    // Inicializar tooltips
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('#myTable').DataTable();
        });
    </script>
<?= $this->endSection() ?>


