<?php

/* 
 * 
 */

?>

<?= $this->extend('plantillas/adminlte_navbar') ?>

<?= $this->section('css')?>
    <?php echo $this->include('Solicitud/tempus_css') ?>
<?= $this->endSection()?>

<?= $this->section('js')?>
    <?= $this->include('Solicitud/tempus_js') ?>
<?= $this->endSection()?>


<?= $this->section('title') ?>
<?= $titulo ?>
<?= $this->endSection() ?>



<?= $this->section('content') ?>
<style>
    .error {
        color: red;
        font-weight: normal;
    }
    label.error {
        color: red;
        font-weight: normal;
        margin-top: 5px;
    }
    input.error, select.error, textarea.error {
        border-color: red;
        background-color: #ffffcc;
    }
</style>

<?= $this->section('content') ?>
<div class="container">
    <h2><?= $titulo ?></h2>

    <?php if (session()->getFlashdata('errors')): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach (session()->getFlashdata('errors') as $error): ?>
                    <li><?= $error ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?= form_open('solicitud/create', ['id' => 'formSolicitud']) ?>
    <div class="form-group">
        <?= form_label('Fecha Solicitud:', 'Fecha_Solicitud') ?>
        <?= form_input('Fecha_Solicitud', date('Y-m-d H:i:s'), ['id' => 'Fecha_Solicitud', 'class' => 'form-control', 'readonly' => 'readonly']) ?>
    </div>
    <div class='row'>
        <div class='col-md-5'>
            <div class="form-group">
                <?= form_label('Fecha Inicio:', 'Fecha_Inicio') ?>
                <div class="input-group date" id="datetimepicker7" data-target-input="nearest">
                    <?= form_input('Fecha_Inicio', set_value('Fecha_Inicio', ''), ['id' => 'Fecha_Inicio', 'class' => 'form-control datetimepicker-input', 'data-target' => '#datetimepicker7', 'required' => 'required']) ?>
                    <div class="input-group-append" data-target="#datetimepicker7" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class='col-md-5'>
            <div class="form-group">
                <?= form_label('Fecha Fin:', 'Fecha_Fin') ?>
                <div class="input-group date" id="datetimepicker8" data-target-input="nearest">
                    <?= form_input('Fecha_Fin', set_value('Fecha_Fin', ''), ['id' => 'Fecha_Fin', 'class' => 'form-control datetimepicker-input', 'data-target' => '#datetimepicker8', 'required' => 'required']) ?>
                    <div class="input-group-append" data-target="#datetimepicker8" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('Ubicación Evento:', 'Ubicacion_Evento') ?>
        <?= form_input('Ubicacion_Evento', set_value('Ubicacion_Evento', ''), ['id' => 'Ubicacion_Evento', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Descripción:', 'Descripcion') ?>
        <?= form_textarea('Descripcion', set_value('Descripcion', ''), ['id' => 'Descripcion', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Importe Bruto:', 'Importe_Bruto') ?>
        <?= form_input('Importe_Bruto', set_value('Importe_Bruto', ''), ['id' => 'Importe_Bruto', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Porcentaje IVA:', 'Porcentaje_IVA') ?>
        <?= form_input('Porcentaje_IVA', '21.00', ['id' => 'Porcentaje_IVA', 'class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Importe IVA:', 'Importe_IVA') ?>
        <?= form_input('Importe_IVA', set_value('Importe_IVA', ''), ['id' => 'Importe_IVA', 'class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Porcentaje IRPF:', 'Porcentaje_IRPF') ?>
        <?= form_dropdown('Porcentaje_IRPF', ['2' => '2%', '7' => '7%', '15' => '15%', '19' => '19%', '21' => '21%', '25' => '25%', '30' => '30%'], '15', ['id' => 'Porcentaje_IRPF', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Importe IRPF:', 'Importe_IRPF') ?>
        <?= form_input('Importe_IRPF', set_value('Importe_IRPF', ''), ['id' => 'Importe_IRPF', 'class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Importe Nómina:', 'Importe_Nomina') ?>
        <?= form_input('Importe_Nomina', set_value('Importe_Nomina', ''), ['id' => 'Importe_Nomina', 'class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Cuota Servicio:', 'Cuota_Servicio') ?>
        <?= form_input('Cuota_Servicio', set_value('Cuota_Servicio', ''), ['id' => 'Cuota_Servicio', 'class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>
    <div class="form-group">
        <?= form_label('CIF Promotor:', 'CIF') ?>
        <?= form_input('CIF', set_value('CIF', ''), ['id' => 'CIF', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Id Promotor Solicitud:', 'Id_Promotor_Solicitud') ?>
        <?= form_input('Id_Promotor_Solicitud', set_value('Id_Promotor_Solicitud', ''), ['id' => 'Id_Promotor_Solicitud', 'class' => 'form-control', 'readonly' => 'readonly']) ?>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Guardar</button>
        <a href="<?= previous_url() ?>" class="btn btn-danger">Cancelar</a>
    </div>
    <?= form_close() ?>
</div>
<?= $this->endSection() ?>
<!-- js necesario -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<?= $this->section('js') ?>
<!-- jQuery Validation Plugin -->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
<!-- Bootstrap JS con Popper.js incluido -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
<!-- DateTime Picker -->
<script src="https://cdn.jsdelivr.net/npm/tempusdominus-bootstrap-4@5.39.0/build/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tempusdominus-bootstrap-4@5.39.0/build/css/tempusdominus-bootstrap-4.min.css" />
<?= $this->endSection() ?>
<script>
    $(function () {

        function calculateValues() {
            let importeBruto = parseFloat($('#Importe_Bruto').val()) || 0;
            let porcentajeIVA = 21;
            let porcentajeIRPF = parseFloat($('#Porcentaje_IRPF').val()) || 0;
            let cuotaServicio = (importeBruto * 0.05).toFixed(2);
            let importeIVA = (importeBruto * (porcentajeIVA / 100)).toFixed(2);
            let importeIRPF = (importeBruto * (porcentajeIRPF / 100)).toFixed(2);

            // Calcular días de alta
            let fechaInicio = $('#Fecha_Inicio').val();
            let fechaFin = $('#Fecha_Fin').val();
            let diasAlta = 1;
            if (fechaInicio && fechaFin) {
                let startDate = new Date(fechaInicio);
                let endDate = new Date(fechaFin);
                diasAlta = (endDate - startDate) / (1000 * 60 * 60 * 24) + 1;
            }

            let costeSeguridadSocial = (importeBruto * 0.29 * diasAlta).toFixed(2);
            let importeNomina = (importeBruto - importeIRPF - cuotaServicio - costeSeguridadSocial).toFixed(2);

            $('#Importe_IVA').val(importeIVA);
            $('#Cuota_Servicio').val(cuotaServicio);
            $('#Importe_IRPF').val(importeIRPF);
            $('#Importe_Nomina').val(importeNomina);
        }

        $('#Importe_Bruto, #Porcentaje_IRPF, #Fecha_Inicio, #Fecha_Fin').on('input change', calculateValues);

        

        $('#CIF').on('blur', function() {
            let cif = $(this).val();
            if (cif.length >= 9) {
                $.ajax({
                    url: '<?= base_url('promotor/buscarPorCIF') ?>',
                    type: 'GET',
                    data: { cif: cif },
                    success: function(response) {
                        if (response.status === 'success') {
                            $('#Id_Promotor_Solicitud').val(response.data.Id_Promotor);
                        } else {
                            $('#Id_Promotor_Solicitud').val('');
                            alert(response.message);
                        }
                    },
                    error: function() {
                        $('#Id_Promotor_Solicitud').val('');
                        alert('Error al buscar el promotor');
                    }
                });
            }
        });
    });
</script>
<?= $this->endSection() ?>