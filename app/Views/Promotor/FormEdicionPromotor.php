<?php
/**
 * Description of FormEdicionPromotor
 *
 * @author paula
 */
?>

<?= $this->extend('plantillas/adminlte_navbar') ?>

<?= $this->section('title') ?>
<?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
    <h2><?= $titulo ?></h2>

    <?php if (isset($errores) && !empty($errores)): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errores as $error): ?>
                    <li><?= $error ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?= form_open('promotor/editar/' . $promotor->Id_Promotor) ?>
    <div class="form-group">
        <?= form_label('Razón Social:', 'Razon_Social') ?>
        <?= form_input('Razon_Social', set_value('Razon_Social', esc($promotor->Razon_Social ?? '')), ['id' => 'Razon_Social', 'class' => 'form-control', 'required' => 'required']) ?>
        <span id="Razon_SocialHelp" class="form-text text-muted text-sm">
            Introduce el <strong>nombre completo de la entidad</strong> 
            en los casos en que el promotor sea una <strong>persona jurídica</strong>.
        </span>
    </div>
    <div class="form-group">
        <?= form_label('Nombre:', 'nombre') ?>
        <?= form_input('nombre', set_value('nombre', esc($promotor->Nombre ?? '')), ['id' => 'nombre', 'class' => 'form-control', 'required' => 'required']) ?>
        <span id="NombreHelp" class="form-text text-muted text-sm">Introduce el <strong>nombre</strong> del promotor <u>solo</u> si se trata de una <strong>persona física</strong>.</span>
    </div>
    <div class="form-group">
        <?= form_label('1er Apellido:', 'Apellido1') ?>
        <?= form_input('Apellido1', set_value('Apellido1', esc($promotor->Apellido1 ?? '')), ['id' => 'Apellido1', 'class' => 'form-control', 'required' => 'required']) ?>
        <span id="Apellido1Help" class="form-text text-muted text-sm">Introduce el <strong>1er Apellido</strong> del promotor <u>solo</u> si se trata de una <strong>persona física</strong>.</span>
    </div>
    <div class="form-group">
        <?= form_label('2º Apellido:', 'apellido2') ?>
        <?= form_input('apellido2', set_value('apellido2', esc($promotor->Apellido2 ?? '')), ['id' => 'apellido2', 'class' => 'form-control']) ?>
        <span id="Apellido2Help" class="form-text text-muted text-sm">Introduce el <strong>2º Apellido</strong> del promotor <u>solo</u> si se trata de una <strong>persona física</strong>.</span>
    </div>
    <div class="form-group">
        <?= form_label('Email:', 'email') ?>
        <?= form_input('email', set_value('email', esc($promotor->Email ?? '')), ['id' => 'email', 'class' => 'form-control', 'type' => 'email', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('CIF:', 'cif') ?>
        <?= form_input('cif', set_value('cif', esc($promotor->CIF ?? '')), ['id' => 'cif', 'class' => 'form-control', 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Teléfono:', 'telefono') ?>
        <?= form_input('telefono', set_value('telefono', esc($promotor->Telefono)), ['id' => 'telefono', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Calle:', 'calle') ?>
        <?= form_input('calle', set_value('calle', esc($promotor->Calle)), ['id' => 'calle', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Número:', 'numero') ?>
        <?= form_input('numero', set_value('numero', esc($promotor->Numero)), ['id' => 'numero', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Código Postal:', 'codigo_postal') ?>
        <?= form_input('codigo_postal', set_value('codigo_postal', esc($promotor->Codigo_Postal)), ['id' => 'codigo_postal', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Localidad:', 'localidad') ?>
        <?= form_input('localidad', set_value('localidad', esc($promotor->Localidad)), ['id' => 'localidad', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">
            Guardar
        </button>
        <a href="<?= base_url('/promotores') ?>" class="btn btn-danger"  data-toggle="tooltip"
           data-placement="right" title="Volver a la tabla de Promotores sin guardar los cambios de la edición">
            Cancelar
        </a>

    </div>
    <?= form_close() ?>
</div>


<script>
    // Inicializar popover
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    };
</script>
<?= $this->endSection() ?>