<?php 
/*******************************************************************************
 * Vista de los PROMOTORES de la BD
 * 
 ******************************************************************************/
?>
<!-- Extendemos la plantilla deseada -->
<?= $this->extend('plantillas/adminlte') ?>

<!-- CSS DataTables -->
<?= $this->section('css') ?>
    <?= $this->include('common/datatables') ?>
    <?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>

<!--  Tïtulo de la página, y la llamaremos en la plantilla -->
<?= $this->section('page_title') ?>
<?=$titulo ?>
<?= $this->endSection() ?>

<?= $this->section('side_bar') ?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?= $this->include('common/sidebar') ?>
</aside>
<?= $this->endSection() ?>

<!-- Card -->
<?= $this->section('subtitle') ?>
Tabla de promotores registrados
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<table id="myTable" class="table table-stripped table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nombre / Razón Social</th>
            <th>CIF</th>
            <th>E-mail</th>
            <th>Teléfono</th>
            <th>Calle</th>
            <th>Código Postal</th>
            <th>Localidad</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($promotores as $promotor): ?>
            <tr>
                <td><?= $promotor->Id_Promotor ?></td>
                <td><?= $promotor->Razon_Social ?> </td>
                <td><?= $promotor->CIF ?></td>
                <td><?= $promotor->Email ?></td>
                <td><?= $promotor->Telefono ?></td>
                <td><?= $promotor->Calle?>, <?= $promotor->Numero?></td>
                <td><?= $promotor->Codigo_Postal?></td>
                <td><?= $promotor->Localidad?></td>
                <td class="text-center">
                    <a href="<?= site_url('promotor/editar/' . $promotor->Id_Promotor) ?>"
                    data-toggle="tooltip" title="Editar Promotor" class="mr-2">
                        <span class="fa-solid fa-pencil text-info">
                        </span>
                    </a>
                    <a href="<?= site_url('artista/borrar/' . $promotor->Id_Promotor) ?>" 
                    onclick="return confirm('¿Estás seguro de que quieres borrar el Promotor seleccionado? Esta acción es irreversible')"
                    data-toggle="tooltip" title="Borrar Promotor" class="ml-2">
                        <span class="fa fa-trash text-danger">
                        </span>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<?= $this->endSection() ?>

<!-- JS DataTables -->
<?= $this->section('js') ?>
    <?= $this->include('common/datatables') ?>
    <?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>