<?php
/**
 * Description of FormAltaPromotor
 * FORMULARIO PARA CREAR/DAR DE ALTA NUEVOS PROMOTORES
 * @author paula
 */ ?>
<?= $this->extend('plantillas/adminlte_navbar') ?>

<?= $this->section('title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

<?= form_open('/promotor/registrar') ?>
<div class="container">
    <h2><?= $titulo ?></h2>

    <div id="confirmacion" class="alert alert-success" style="display: none;">
        ¡Registro correcto! El promotor ha sido registrado con éxito.
    </div>

    <div id="errores" class="alert alert-danger" style="display: none;">
        <ul id="listaErrores"></ul>
    </div>

    <div class="form-group">
        <?= form_label('Tipo de Entidad', 'Tipo_Entidad') ?>
        <?= form_dropdown('Tipo_Entidad', ['' => 'Seleccionar', 'Fisica' => 'Fisica', 'Juridica' => 'Juridica'], set_value('tipo_entidad'), ['id' => 'tipo_entidad', 'class' => 'form-control', 'required' => 'required']) ?>
        <span id="TipoEntidadHelp" class="form-text text-muted text-sm">Elegir el tipo determina los campos a rellenar.</span>
    </div>
    <div class="form-group">
        <?= form_label('Nombre', 'Nombre') ?>
        <?= form_input('Nombre', set_value('nombre'), ['id' => 'nombre', 'class' => 'form-control']) ?>
        <span id="NombreHelp" class="form-text text-muted text-sm">Solo para personas físicas.</span>
    </div>
    <div class="form-group">
        <?= form_label('1er Apellido', 'Apellido1') ?>
        <?= form_input('Apellido1', set_value('apellido1'), ['id' => 'apellido1', 'class' => 'form-control']) ?>
        <span id="Apellido1Help" class="form-text text-muted text-sm">Solo para personas físicas.</span>
    </div>
    <div class="form-group">
        <?= form_label('2º Apellido', 'Apellido2') ?>
        <?= form_input('Apellido2', set_value('apellido2'), ['id' => 'apellido2', 'class' => 'form-control']) ?>
        <span id="Apellido2Help" class="form-text text-muted text-sm">Solo para personas físicas.</span>
    </div>
    <div class="form-group">
        <?= form_label('Razón Social', 'Razon_Social') ?>
        <?= form_input('Razon_Social', set_value('razon_social'), ['id' => 'razon_social', 'class' => 'form-control']) ?>
        <span id="RazonSocialHelp" class="form-text text-muted text-sm">Solo para personas jurídicas.</span>
    </div>
    <div class="form-group">
        <?= form_label('CIF', 'CIF') ?>
        <?= form_input('CIF', set_value('cif'), ['id' => 'cif', 'class' => 'form-control', 'required' => 'required']) ?>
        <span id="CIFHelp" class="form-text text-muted text-sm">Requerido para ambos tipos de entidad.</span>
    </div>
    <div class="form-group">
        <?= form_label('Teléfono', 'Telefono') ?>
        <?= form_input('Telefono', set_value('telefono'), ['id' => 'telefono', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Email', 'Email') ?>
        <?= form_input('Email', set_value('email'), ['id' => 'email', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Calle', 'Calle') ?>
        <?= form_input('Calle', set_value('calle'), ['id' => 'calle', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Número', 'Número') ?>
        <?= form_input('Número', set_value('numero'), ['id' => 'numero', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Código Postal', 'Codigo_Postal') ?>
        <?= form_input('Codigo_Postal', set_value('codigo_postal'), ['id' => 'codigo_postal', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= form_label('Localidad', 'Localidad') ?>
        <?= form_input('Localidad', set_value('localidad'), ['id' => 'localidad', 'class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Registrar</button>
        <a href="<?= base_url('/promotores') ?>" class="btn btn-danger">Cancelar</a>
    </div>
<?= form_close() ?>

<!-- js necesario ??? -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

  <!-- Muestra los errores si existen -->
<?php if(isset($errors)): ?>
    <div class="alert alert-danger">
        <?php foreach ($errors as $error): ?>
            <p><?= esc($error) ?></p>
        <?php endforeach ?>
    </div>
<?php endif ?>
<?= $this->endSection() ?>


