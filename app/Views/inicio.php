<?php
/* * *****************************************************************************
 * Ejemplo de VISTA que utiliza la PLANTILLA de adminlte 
 * que está en app\Views\plantillas\adminlte.php
 * **************************************************************************** */
?>

<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('css') ?>
<!-- CSS personalizado para escala de grises en los logos-->
<link rel="stylesheet" href="<?= base_url('assets/css/inicio.css') ?>">
<?= $this->endSection() ?>


<?= $this->section('side_bar') ?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?= $this->include('common/sidebar') ?>
</aside>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
Bienvenidos a Canon
<?= $this->endSection() ?>



<?= $this->section('subtitle') ?>
Tu asesoría especializada en artistas y técnicos en espectáculos públicos
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div>
    <h2 class="text-center">Nuestros servicios</h2>

    <div class="row">
        <!--<div class="col-md-4">
            <a href="<?= site_url('/registrar') ?>" class="text-decoration-none">
                <div class="info-box">
                    <span class="info-box-icon bg-success"><i class="fas fa-user-plus"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Date de alta</span>
                        <span class="info-box-number">Regístrate aquí</span>
                    </div>
                </div>
            </a>
        </div>-->

        <div class="col-md-6">
            <a href="<?= site_url('/login') ?>" class="text-decoration-none">
                <div class="info-box">
                    <span class="info-box-icon bg-indigo"><i class="fas fa-arrow-right-to-bracket"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Login: inicia sesión para todas las funcionalidades</span>
                        <span class="info-box-number">Usuarios registrados</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-6">
            <a href="<?= site_url('/quienes_somos') ?>" class="text-decoration-none">
                <div class="info-box">
                    <span class="info-box-icon bg-olive"><i class="fas fa-circle-info"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Quiénes somos</span>
                        <span class="info-box-number">Conócenos</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <!-- segunda fila -->
    <div class="row">
        <div class="col-md-4">
            <a href="<?= site_url('/contacto') ?>" class="text-decoration-none">
                <div class="info-box">
                    <span class="info-box-icon bg-lightblue"><i class="fas fa-envelope"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Contactar</span>
                        <span class="info-box-number">Envíanos un mensaje</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?= site_url('/simulador') ?>" class="text-decoration-none">
                <div class="info-box">
                    <span class="info-box-icon bg-maroon"><i class="fas fa-euro-sign"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Simulador</span>
                        <span class="info-box-number">¿Cuánto cobraré?</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?= site_url('/preguntas_frecuentes') ?>" class="text-decoration-none">
                <div class="info-box">
                    <span class="info-box-icon bg-info"><i class="fas fa-question"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">¿Dudas?</span>
                        <span class="info-box-number">Preguntas frecuentes</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <!-- Sección "Qué opinan de nosotros" -->
    <div class="container mt-5">
        <h2 class="text-center">Qué opinan de nosotros</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="card my-3">
                    <div class="card-body">
                        <p class="card-text">"Excelente servicio y atención al cliente. Siempre dispuestos a ayudar."</p>
                        <p class="text-warning">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </p>
                        <h5 class="card-title"> Jaime - Cantante de RockyRacoon</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card my-3">
                    <div class="card-body">
                        <p class="card-text">"Una empresa confiable y profesional. Los recomiendo."</p>
                         <p class="text-warning">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star-half-alt"></i>
                        </p>
                        <h5 class="card-title">Lara - Teclista de FunkyBaras</h5>
                       
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card my-3">
                    <div class="card-body">
                        <p class="card-text">"Han facilitado enormemente mi gestión laboral y fiscal. Un aliado imprescindible."</p>
                        <p class="text-warning">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </p>
                        <h5 class="card-title">Lucía - técnico y artista</h5>
                    </div>
                </div>
            </div>
            <!-- Sección "Algunos de nuestros clientes" -->
            <div class="container mt-5">
                <h2 class="text-center">Algunos de nuestros clientes</h2>
                <div class="row justify-content-center">
                    <div class="col-md-2 col-4 my-2">
                        <img src="assets/images/rockyracoon.png" alt="Rocky Racoon" class="img-fluid grayscale fixed-size">
                    </div>
                    <div class="col-md-2 col-4 my-2">
                        <img src="assets/images/funkybaras.png" alt="Funkybaras" class="img-fluid grayscale fixed-size">
                    </div>
                    <div class="col-md-2 col-4 my-2">
                        <img src="assets/images/alboraya-escudo.jpg" alt="AyuntamientoAlboraya" class="img-fluid grayscale fixed-size">
                    </div>
                    <div class="col-md-2 col-4 my-2">
                        <img src="assets/images/luciazambudio-logo.png" alt="LZ" class="img-fluid grayscale fixed-size">
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

</div>
<?= $this->endSection() ?>

<!-- JS opcional -->
<?= $this->section('js') ?>
<?= $this->include('common/bootstrap') ?>
<?= $this->endSection() ?>