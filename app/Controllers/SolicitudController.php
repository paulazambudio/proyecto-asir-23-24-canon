<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

use App\Models\SolicitudModel;
use App\Models\PromotorModel;
use DateTime;

/**
 * Description of SolicitudController
 *
 * @author paula
 */
class SolicitudController extends BaseController {
    /*
     *  MOSTRAR todas las solicitudes
     */

    public function index() {
        // Crear una instancia del modelo
        $solicitudes = new \App\Models\SolicitudModel();
        // Pasar "Título" a la variable de datos de la vista
        $data['titulo'] = 'Solicitudes';

        /* DIFERENTES VISTAS PARA DIFERENTES GRUPOS DE USUARIOS */
        //primero comprobar si el usuario ha iniciado sesion para usar su user_id
        // Verificar si el usuario ha iniciado sesión
        if (!auth()->loggedIn()) {
            // Si el usuario no ha iniciado sesión, redirigir a la página de login
            return redirect()->to('/login');
        }

        // Obtener el ID del usuario actual y sus grupos
        $usuarioActual = auth()->user();
        $usuarioActualId = $usuarioActual->id;

        // Comprobar grupo y devolver las solicitudes correspondientes
        if ($usuarioActual->inGroup('admin') || $usuarioActual->inGroup('superadmin')) {
            // Si el usuario es admin o superadmin, obtener TODAS las solicitudes
            $data['solicitudes'] = $solicitudes->findAll();
        } elseif ($usuarioActual->inGroup('artista')) {
            // Si el usuario es artista, obtener solo SUS solicitudes con el método del MODELO
            $data['solicitudes'] = $solicitudes->obtenerSolicitudesParaArtista($usuarioActualId);
        } else {
            // Si el usuario no pertenece a ninguno de los grupos mencionados, mostrar mensaje de error
            $data['solicitudes'] = [];
            $data['error'] = 'No tienes permisos para ver esta información.';
        }

        // Cargar la vista con los datos
        return view('/Solicitud/SolicitudTabla', $data);
    }

//    public function add(){
//        $fecha = DateTime::createFromFormat("d/m/Y",$datos['fecha']); //formato dd/mm/aaaa vide de getPost('fecha')
//        $datos['fecha'] = $fecha->format("Y-m-d");  //lo pasas a formato aaa-mm-dd (mysql)
//    }

    /**
     * CREAR NUEVA SOLICITUD
     */
    // crear el formulario
    public function create() {
        $data['titulo'] = 'Crear Solicitud';
        echo view('Solicitud/FormNuevaSolicitud', $data);
    }

    //gestión del formulario: traer datos del modelo, calcular e insertar
    public function store() {
        //inicializamos el modelo
        $model = new SolicitudModel();
        //obtenemos datos del formulario
        $data = $this->request->getPost();

        //quitamos lo que hemos obtenido de la función de búsqueda
        //porque ésto no ha de ser insertado a la base de datos, sino su valor
        unset($data['buscador']);

        // Obtener el ID del artista desde la sesión
        $data['Id_Artista_Solicitud'] = user_id();

        // Calculamos los valores no ingresados por el usuario 
        $data['Fecha_Solicitud'] = date('Y-m-d H:i:s'); //establece en fecha y hora actual
        $data['Porcentaje_IVA'] = 21; //iva fijo en 21%
        $data['Importe_IVA'] = $data['Importe_Bruto'] * ($data['Porcentaje_IVA'] / 100);
        $data['Cuota_Servicio'] = $data['Importe_Bruto'] * 0.05;
        $data['Importe_IRPF'] = $data['Importe_Bruto'] * ($data['Porcentaje_IRPF'] / 100);

        // Calcular días de alta - formateados para MySQL
        $fechaInicio = DateTime::createFromFormat("d/m/Y", $data['Fecha_Inicio']);
        $data['Fecha_Inicio'] = $fechaInicio->format("Y-m-d");
        $fechaFin = DateTime::createFromFormat("d/m/Y", $data['Fecha_Fin']);
        $data['Fecha_Fin'] = $fechaFin->format("Y-m-d");
        // sumar el diferencial para obtener el total de días
        $diasAlta = $fechaFin->diff($fechaInicio)->days + 1;

        //cálculo del importe de la nómina con más datos no visibles para el usuario
        $costeSeguridadSocial = $data['Importe_Bruto'] * 0.29 * $diasAlta;
        $data['Importe_Nomina'] = $data['Importe_Bruto'] - $costeSeguridadSocial - $data['Importe_IRPF'] - $data['Cuota_Servicio'];

        //depurador
//        echo '<pre>';
//        echo $diasAlta;
//        print_r($data);
        //Insertar datos en el modelo
        if ($model->insert($data)) {
            $id = $model->insertID(); // Obtener el ID de la solicitud insertada
            return redirect()->to('/nueva_solicitud/detalle/' . $id)->with('message', 'Solicitud creada exitosamente');
        } else {
            return redirect()->back()->withInput()->with('errors', $model->errors());
        }
    }

    /*
     * VISTA DETALLE DE CADA SOLICITUD
     */

    public function detalle($id) {
        //nueva instancia de los 2 modelos necesarios para esta vista: Solicitud y Promotor
        $model = new SolicitudModel();
        $promotorModel = new PromotorModel();
        
        //buscamos la solicitud por su id en el modelo y lo almacenamos en la variable
        $solicitud = $model->find($id);

        //verificación de su existencia con mensaje de error personalizado
        if (!$solicitud) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException("No se encontró la solicitud con ID $id");
        }

        // Obtener información del promotor: asignamos el id
        $promotor = $promotorModel->find($solicitud->Id_Promotor_Solicitud);
        
        // preparación de los datos para la vista, con las variables de los modelos
        $data = [
            'titulo' => 'Detalle de la Solicitud',
            'solicitud' => $solicitud,
            'promotor' => $promotor ?? null
        ];
        //renderizamos la vista
        return view('Solicitud/SolicitudDetalle', $data);
    }

    /*     * ********************
     * FUNCIÓN DE BÚSQUEDA 
     * se encarga de buscar promotores y devolver un select html con lo encontrado
     * ********************** */

    public function buscar() {
        //obtiene el texto de búsqueda
        $texto = $this->request->getPost('texto');
        //desde el Modelo de los promotores, es decir, la BD, se buscan los promotores que coincidan con el texto
        $modelpromotor = new PromotorModel();
        //echo '<pre>';
        $promotores = $modelpromotor->buscar($texto);
        //se genera html para el SELECT que muestre hasta 4 opciones visibles
        //NOTA: no se inserta el texto ni la razón social -> el valor es Id_Promotor porque es la FK de la tabla 
        $html = '<select name="Id_Promotor_Solicitud" class="form-control" size="4">';
        foreach ($promotores as $promotor) {
            $html .= "<option value='$promotor->Id_Promotor'>";
            $html .= $promotor->Razon_Social;
            $html .= '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    /*     * *********
     * EDICIÓN
     * ********* */

    public function mostrarFormularioEdicion($id) {
        helper('form');
        $solicitudModel = new SolicitudModel();
        $solicitud = $solicitudModel->find($id);
        if (!$solicitud) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Solicitud con ID ' . $id . ' no encontrada.');
        }
        $data = [
            'titulo' => 'Editar Solicitud',
            'solicitud' => $solicitud
        ];
        return view('Solicitud/FormEdicionSolicitud', $data);
    }

    // Método para PROCESAR el formulario de EDICIÓN (POST)
    public function procesarFormularioEdicion($id) {
        $solicitudModel = new SolicitudModel();
        $solicitud = $solicitudModel->find($id);
        $data = [
            'titulo' => 'Editar Solicitud',
            'solicitud' => $solicitud
        ];
        if (strtoupper($this->request->getMethod()) === 'POST') {
            $solicitudData = $this->request->getPost();
            $solicitudData['Id_Solicitud'] = $id;
            if ($solicitudModel->save($solicitudData) === false) {
                $errors = $data['errors'] = $solicitudModel->errors();
                return view('Solicitud/FormEdicionSolicitud', $data);
            } else {
                return redirect()->to('/solicitudes')->with('message', 'Solicitud actualizada exitosamente.');
            }
        } else {
            return redirect()->to('/');
        }
    }

    /*
     * BORRADO DE SOLICITUDES
     */

    public function borrar($Id_Solicitud) {
        $solicitudes = new SolicitudModel();
        $solicitudes->delete($Id_Solicitud);

        return redirect()->to('/solicitudes')->with('message', 'Solicitud borrada exitosamente.');
    }
}
