<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function bienvenida(){
        $data['title']= 'Bienvenidos a Canon';

        //extender la vista
        return view('inicio', $data);
    }
}