<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

namespace App\Controllers;


class Inicio extends BaseController
{
    //esta es la función encargada de la ruta "/" de la web
    public function bienvenida(){
        $data['title']= 'Bienvenidos a Canon';
        $data['subtitulo']= 'Conoce nuestros servicios';
        //verificamos si está loggeado para llevarlo al área interna
        if (auth()->loggedIn()) {
            //para depurar intentamos comprobar con método de shield el id de usuario
            //echo '<pre>';
            //echo user_id();
            
            //esta es la vista del área interna
            $data['title']='Completa el registro'; 
            return view('registro',$data); 
        }
        //si no está loggeado, vendrá a parar aquí
        return view('/inicio', $data);
    }
    
    public function registro() {
        $data['title']='Completa el registro'; 
        return view('registro',$data); 
    }

}
