<?php
/********************OJO
 * aunque salga el warning de que con la  A mayúscula no se corresponde 
 * con la estructura de carpetas actual,  dejarlo con App 
 * porque con app da fallo  
 ***********************/
namespace App\Controllers;

class Prueba extends BaseController
{
    public function inicio(){
        // Establecer la configuración regional a español
        setlocale(LC_TIME, 'es_ES.UTF-8');
        /* Función date, fecha y hora en cierto formato
         Abajo Función strftime que es algo diferente, no solo en formato
         Almacenamos en variables*/
        $hoy = date('l d-m-Y, g:i a');
        $ahora = strftime("%A, %e de %B, %H:%M:%S horas");
        // Mostramos 
        echo "Hola";
        echo "<br>";
        echo "Hoy es $hoy";
        echo "<br>";
        echo "Ahora es $ahora";
        echo "<br>";
        echo "Adiós";
    }
    
}