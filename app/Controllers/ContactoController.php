<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of ContactoController
 *
 * @author paula
 */
class ContactoController extends BaseController {
    public function contacto(){
	$data['title'] = "Contacto";
	return view('/contacto',$data);
    }
}
