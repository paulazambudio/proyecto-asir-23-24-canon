<?php

namespace App\Controllers;
/**
 * Description of QuienesSomosController
 *
 * @author paula
 */
class QuienesSomosController extends BaseController {
   
    public function quienessomos() {
        $data['titulo'] = 'Quiénes somos';
        return view('QuienesSomos', $data);
    }
}
