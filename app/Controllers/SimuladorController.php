<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of SimuladorController
 *
 * @author paula
 */
class SimuladorController extends BaseController{
    public function simulador(){
	helper('form');
	$data['title'] = "Simulador de salario";
	return view('/Simulador/Simulador',$data);
    }
}
