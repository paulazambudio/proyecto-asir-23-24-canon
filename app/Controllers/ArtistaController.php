<?php

namespace App\Controllers;

use App\Models\ArtistaModel;

/**
 * Description of ArtistaController
 *
 * @author paula
 */
class ArtistaController extends BaseController {

    // función/método MOSTRARÁ todos los artistas de la tabla Artista de la BD
    public function index() {
        // Crear una instancia del modelo
        $artistas = new ArtistaModel();
        // Pasar "Título" a la variable de datos de la vista
        $data['titulo'] = 'Artistas';
        // Obtener todos los artistas
        $data['artistas'] = $artistas->findAll();
        //comprobación: busca/encuentra el artista cuyo id=1
        //print_r($artistas->find(1)); 
        // Cargar la vista con los datos
        return view('/Artista/Tabla', $data);
    }

    /*     * **************************
     * ALTA/COMPLETAR REGISTRO DE ARTISTAS
     * ************************* */

    // Método para mostrar el formulario de registro
    public function mostrarFormularioRegistro() {
        // Cargar el helper 'form' para utilizar funciones relacionadas con formularios
        helper('form');
        $userId = user_id();

        // Crear una instancia del modelo ArtistaModel
        $artistaModel = new ArtistaModel();
        
        // Comprobar si el usuario ya tiene un registro en la tabla Artista
        $artista = $artistaModel->where('id_user', $userId)->first();

        if ($artista) {
            // Si el artista ya existe, redirigir a la página de edición
            return redirect()->to("/artista/editar/{$artista->Id_Artista}")->with('info', 'Ya tienes un perfil de artista. Puedes editarlo aquí.');
        }

        // Si no existe, mostrar el formulario de registro
        $data['titulo'] = 'Registro de Artista (nuevo alta en el sistema)';
        $data['artista'] = new \stdClass(); // Pasar un objeto vacío a la vista
        return view('Artista/FormAltaArtista', $data);
    }

    // Método para PROCESAR el formulario ENVIADO (POST)
    public function procesarFormularioRegistro() {
        $artistaModel = new ArtistaModel(); // Crear una instancia del modelo
        // Verificar si el método de la solicitud es POST
        if (strtoupper($this->request->getMethod()) === 'POST') {
            // Obtener el ID del usuario desde la sesión
            $userId = user_id();
            // Obtener los datos del formulario
            $artistaData = [
                'Nombre' => $this->request->getPost('Nombre'),
                'Apellido1' => $this->request->getPost('Apellido1'),
                'Apellido2' => $this->request->getPost('Apellido2'),
                'Dni' => $this->request->getPost('Dni'),
                'Nuss' => $this->request->getPost('Nuss'),
                'Cuenta_Corriente' => $this->request->getPost('Cuenta_Corriente'),
                'Telefono' => $this->request->getPost('Telefono'),
                'Calle' => $this->request->getPost('Calle'),
                'Numero' => $this->request->getPost('Numero'),
                'Codigo_Postal' => $this->request->getPost('Codigo_Postal'),
                'Localidad' => $this->request->getPost('Localidad'),
                'id_user' => $userId // Asociar el artista con el usuario autenticado
            ];

            // VALIDACIÓN de los datos del formulario
            if (!$this->validate($artistaModel->getValidationRules(), $artistaModel->getValidationMessages())) {
                $data['titulo'] = 'Registro de Artista (nuevo alta en el sistema)'; // Título de la vista
                $data['errors'] = $this->validator->getErrors(); // Obtener errores de validación
                return view('Artista/FormAltaArtista', $data); // Volver a cargar la vista con errores
            }

            // Depurar: Imprimir los datos enviados
            //echo '<pre>';
            //print_r($artistaData);
            //echo '</pre>';
            // Intentar insertar los datos en la base de datos
            if ($artistaModel->insert($artistaData) === false) {
                $data['titulo'] = 'Registro de Artista (nuevo alta en el sistema)'; // Título de la vista
                $data['errors'] = $artistaModel->errors(); // Obtener errores de inserción
                return view('Artista/FormAltaArtista', $data); // Volver a cargar la vista con errores
            }

            // Si la inserción es exitosa...
            $data['titulo'] = 'Registro de Artista (nuevo alta en el sistema)';
            $data['success'] = '¡Registro correcto! El artista ha sido registrado con éxito.';
            // Obtener el ID del artista recién creado (insertado)
            $nuevoArtistaId = $artistaModel->getInsertID();

            // Redirigir a la nueva vista con el mensaje de éxito
            return redirect()->to("/artista/detalle/$nuevoArtistaId")->with('success', '¡Registro correcto! El artista ha sido registrado con éxito.');
        }
    }

    //detalles del artista reción registrado
    public function detalle($id) {
        $artistaModel = new ArtistaModel();
        $data['artista'] = $artistaModel->find($id);
        //buscarlo por su ID para mostrarlo
        return view('Artista/DetalleArtista', $data);
    }

    /************************
     * EDICIÓN DE ARTISTAS
     ************************/

    // Método para mostrar el formulario de edición
    public function mostrarFormularioEdicion($id) {
        helper('form');
        $artistaModel = new ArtistaModel();
        $artista = $artistaModel->find($id);
        if (!$artista) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Artista con ID ' . $id . ' no encontrado.');
        }
        $data = [
            'titulo' => 'Editar Artista',
            'artista' => $artista
        ];
        return view('Artista/FormEdicionArtista', $data);
    }

    // Método para PROCESAR el formulario de EDICIÓN (POST)
    public function procesarFormularioEdicion($id) {
        $artistaModel = new ArtistaModel();
        $artista = $artistaModel->find($id);
        $data = [
            'titulo' => 'Editar Artista',
            'artista' => $artista
        ];
        if (strtoupper($this->request->getMethod()) === 'POST') {
            $artistaData = $this->request->getPost();
            $artistaData['Id_Artista'] = $id;
            if ($artistaModel->save($artistaData) === false) {
                $errors = $data['errors'] = $artistaModel->errors();
                print_r($data['errors']);
                return view('Artista/FormEdicionArtista', $data);
            } else {
                //echo '<pre>';
                print_r($artistaData);
            }
        } else {// Si la solicitud no es POST, redirigir a la página de inicio u otra página apropiada
            return redirect()->to('/');
        }
    }

    /*     * *******************
     * BORRADO DE ARTISTAS
     * ****************** */

    public function borrar($Id_Artista) {
        $artistas = new ArtistaModel();
        $artistas->delete($Id_Artista);

        return redirect()->to('/artistas');
    }
}
