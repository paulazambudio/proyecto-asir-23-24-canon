<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

use App\Models\FacturaModel;
use App\Models\SolicitudModel;
use App\Models\PromotorModel;
use App\Models\ArtistaModel;

/**
 * Description of FacturaController
 *
 * @author paula
 */
class FacturaController extends BaseController {

    //Mostrar las facturas
    public function index() {
        // Crear una instancia del modelo de facturas
        $facturas = new FacturaModel();
        $data['titulo'] = 'Facturas';

        // Verificar si el usuario ha iniciado sesión
        if (!auth()->loggedIn()) {
            return redirect()->to('/login');
        }

        // Obtener el ID del usuario actual y sus grupos
        $usuarioActual = auth()->user();
        $usuarioActualId = $usuarioActual->id;

        // Comprobar grupo y devolver las facturas correspondientes
        if ($usuarioActual->inGroup('admin') || $usuarioActual->inGroup('superadmin')) {
            // Si el usuario es admin o superadmin, obtener TODAS las facturas
            $data['facturas'] = $facturas->findAll();
        } elseif ($usuarioActual->inGroup('artista')) {
            // Obtener las facturas relacionadas con las solicitudes del artista
            $data['facturas'] = $facturas->select('Factura.*')
                    ->join('Solicitud', 'Solicitud.Id_Solicitud = Factura.Id_Solicitud_Factura')
                    ->where('Solicitud.Id_Artista_Solicitud', $usuarioActualId)
                    ->findAll();
        } else {
            // Si el usuario no pertenece a ninguno de los grupos mencionados, mostrar mensaje de error
            $data['facturas'] = [];
            $data['error'] = 'No tienes permisos para ver esta información.';
        }

        return view('/Factura/FacturaTabla', $data);
    }

    // Busca solicitudes por ID o descripción, o muestra todas las solicitudes 
    // como crear una factura es tarea del admin, solo ellos pueden generar facturas desde solicitudes que todavía no han sido facturadas
    public function buscarSolicitud() {
        // Obtener el texto de búsqueda enviado desde el formulario
        $texto = $this->request->getPost('texto');

        // Crear una instancia del modelo de Solicitud
        $modelSolicitud = new SolicitudModel();

        // Obtener solicitudes sin factura y con los detalles adicionales
        $solicitudes = $modelSolicitud->buscarSolicitudesSinFactura($texto);

        // Generar el HTML para el select
        $html = '<select name="Id_Solicitud_Factura" class="form-control" size="4">';
        foreach ($solicitudes as $solicitud) {
            // Crear una opción para cada solicitud encontrada
            $html .= "<option value='{$solicitud->Id_Solicitud}'>";
            $html .= "<strong>{$solicitud->Id_Solicitud}</strong>";
            $html .= " - <strong>{$solicitud->Nombre_Artista} {$solicitud->Apellido1_Artista} {$solicitud->Apellido2_Artista}</strong>";
            $html .= " - <strong>{$solicitud->Fecha_Inicio}</strong>";
            $html .= " - {$solicitud->Nombre_Promotor}";
            $html .= " - {$solicitud->Ubicacion_Evento}";
            $html .= " - {$solicitud->Descripcion}";
            $html .= '</option>';
        }
        $html .= '</select>';
        return $html; // Devolver el HTML generado
    }

    // Muestra el formulario para crear una nueva factura
    public function create() {
        $data['titulo'] = 'Crear Factura';
        return view('/Factura/FormNuevaFactura', $data);
    }

    // Guarda una nueva factura basada en una solicitud existente
    public function store() {
        $model = new FacturaModel();
        $data = $this->request->getPost();

        // Obtener la solicitud seleccionada
        $modelSolicitud = new SolicitudModel();
        $solicitud = $modelSolicitud->find($data['Id_Solicitud_Factura']);

        // Asignar datos de la solicitud a la factura
        $data['Fecha_Emision'] = date('Y-m-d');
        $data['Estado'] = $data['Estado'] ?? 'Emitida';  // Forzar "emitida" como estado
        $data['Importe_Bruto'] = $solicitud->Importe_Bruto;
        $data['Importe_IVA'] = $solicitud->Importe_Bruto * ($solicitud->Porcentaje_IVA / 100);
        $data['Importe_IRPF'] = $solicitud->Importe_Bruto * ($solicitud->Porcentaje_IRPF / 100);
        $data['Porcentaje_IVA'] = $solicitud->Porcentaje_IVA;
        $data['Porcentaje_IRPF'] = $solicitud->Porcentaje_IRPF;
        $data['Importe_Total_Factura'] = $data['Importe_Bruto'] + $data['Importe_IVA']; // Excluyendo IRPF del cálculo
        $data['Id_Promotor_Factura'] = $solicitud->Id_Promotor_Solicitud;

        // Insertar la factura en la base de datos
        if ($model->insert($data)) {
            $id = $model->insertID();
            return redirect()->to('/factura/detalle/' . $id)->with('message', 'Factura creada exitosamente');
        } else {
            return redirect()->back()->withInput()->with('errors', $model->errors());
        }
    }

    //detalles tras la creación
    public function detalle($id) {
        $model = new FacturaModel();
        $factura = $model->find($id);

        if (!$factura) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Factura no encontrada');
        }

        $modelPromotor = new PromotorModel();
        $promotor = $modelPromotor->find($factura->Id_Promotor_Factura);

        $modelSolicitud = new SolicitudModel();
        $solicitud = $modelSolicitud->find($factura->Id_Solicitud_Factura);

        $data = [
            'titulo' => 'Detalle de Factura',
            'factura' => $factura,
            'promotor' => $promotor,
            'solicitud' => $solicitud,
        ];

        return view('Factura/DetalleFactura', $data);
    }
    
    // Método para mostrar el formulario de edición
    public function mostrarFormularioEdicion($id)
    {
        helper('form');
        $facturaModel = new FacturaModel();
        $factura = $facturaModel->find($id);
        if (!$factura) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Factura con ID ' . $id . ' no encontrada.');
        }

        $modelPromotor = new PromotorModel();
        $promotor = $modelPromotor->find($factura->Id_Promotor_Factura);

        $modelSolicitud = new SolicitudModel();
        $solicitud = $modelSolicitud->find($factura->Id_Solicitud_Factura);

        $data = [
            'titulo' => 'Editar Factura',
            'factura' => $factura,
            'promotor' => $promotor,
            'solicitud' => $solicitud,
        ];
        return view('Factura/FormEdicionFactura', $data);
    }

    // Método para PROCESAR el formulario de EDICIÓN (POST)
    public function procesarFormularioEdicion($id)
    {
        $facturaModel = new FacturaModel();
        $factura = $facturaModel->find($id);
        $data = [
            'titulo' => 'Editar Factura',
            'factura' => $factura
        ];
        if (strtoupper($this->request->getMethod()) === 'POST') {
            $facturaData = $this->request->getPost();
            $facturaData['Id_Factura'] = $id;
            if ($facturaModel->save($facturaData) === false) {
                $data['errors'] = $facturaModel->errors();
                return view('Factura/FormEdicionFactura', $data);
            } else {
                return redirect()->to('/factura/detalle/' . $id)->with('message', 'Factura actualizada exitosamente');
            }
        } else {
            return redirect()->to('/');
        }
    }

    // Método para borrar una factura
    public function borrar($Id_Factura)
    {
        $facturaModel = new FacturaModel();
        $facturaModel->delete($Id_Factura);

        return redirect()->to('/facturas');
    }
    
}
