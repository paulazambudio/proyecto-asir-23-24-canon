<?php

/**
 * Description of TrabajadorController
 * Controlador encargado de manejar la lógica de selección de trabajadores,
 * 
 * @author paula
 */

namespace App\Controllers;
use App\Models\ArtistaModel;
use App\Models\SolicitudModel;


class TrabajadorController extends BaseController{
    // Método para cargar la vista inicial con todos los artistas
    public function index() {
    	// Cargamos el modelo de Artista
    	$artistas = new ArtistaModel();
    	// Obtenemos todos los artistas de la base de datos utilizando findAll()
    	$data['artistas'] = $artistas->findAll();
    	// Título de la página
    	$data['titulo'] = "Seleccionar artistas/trabajadores para dar de alta";
    	// Cargamos la vista pasando los datos de los artistas y el título
    	return view('SeleccionTrabajador', $data);
	}

}
