<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

use App\Models\PromotorModel;

/**
 * Description of PromotorController
 *
 * @author paula
 */
class PromotorController extends BaseController {
    // función/método MOSTRARÁ todos los promotores de la tabla Promotor de la BD
    public function index() {
        $promotores = new PromotorModel();
        $data['titulo'] = 'Promotores';
        $data['promotores'] = $promotores->findAll();

        return view('/Promotor/Tabla', $data);
    }

/****************************
     * ALTA/REGISTRO DE PROMOTORES
     ***************************/
    // Método para mostrar el formulario de registro
    public function mostrarFormularioRegistro() {
        // Cargar el helper 'form' para utilizar funciones relacionadas con formularios
        helper('form');

        // Definir el título para la vista
        $data['titulo'] = 'Registro de Promotor (nuevo alta en el sistema)';

        // Crear una instancia del modelo PromotorModel
        $promotorModel = new PromotorModel();
        $data['promotor'] = $promotorModel; // Pasamos un objeto vacío a la vista

        // Cargar la vista del formulario de registro
        return view('Promotor/FormAltaPromotor', $data);
    }

    // Método para PROCESAR el formulario ENVIADO (POST)
    public function procesarFormularioRegistro() {
        // Cargar el helper 'form' para utilizar funciones relacionadas con formularios
        helper('form');

        // Crear una instancia del modelo PromotorModel
        $promotorModel = new PromotorModel();

        // Verificar si el método de la solicitud es POST
        if (strtoupper($this->request->getMethod()) === 'POST') {
            // Obtener los datos del formulario
            $promotorData = $this->request->getPost();

            // Validar los datos del formulario e insertar en la base de datos
            if ($promotorModel->save($promotorData) === false) {
                // Preparar los datos para la vista: título de la vista, y datos del promotor
                $data = [
                    'titulo' => 'Registro de Promotor (nuevo alta en el sistema)',
                    'promotor' => $promotorModel,
                    'errors' => $promotorModel->errors()
                ];

                // Retornar la vista del formulario con los datos y los errores
                return view('Promotor/FormAltaPromotor', $data);
            } else {
                // Redirigir a la página de lista de promotores o mostrar un mensaje de éxito
                return redirect()->to('/promotores')->with('message', 'Registro exitoso');
            }
        } else {
            // Si la solicitud no es POST, redirigir a la página de inicio u otra página apropiada
            return redirect()->to('/');
        }
    }
    
    /************************
     * EDICIÓN DE PROMOTORES
     ************************/
    // Método para solamente MOSTRAR el formulario de edición
    public function editarPromotor($id) {
        helper('form');
        helper('html');

        $promotorModel = new PromotorModel();
        $promotor = $promotorModel->find($id);

        if (!$promotor) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Promotor con ID ' . $id . ' no encontrado.');
        }

        $data = [
            'titulo' => 'Editar Promotor',
            'promotor' => $promotor
        ];

        return view('Promotor/FormEdicionPromotor', $data);
    }
    // Método para PROCESAR el formulario de EDICIÓN (POST)
    public function actualizarEdicionPromotor($id) {
        //comprobar si el método es POST - si no, salimos de aquí
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('/inicio');
        }

        $promotorModel = new PromotorModel();
        $promotorData = $this->request->getPost();

        // Validar los datos recibido utilizando 1º las reglas de validación del modelo
        if (!$this->validate($promotorModel->getValidationRules())) {
            $errors = $this->validator->getErrors();
            return redirect()->back()->with('errors', $errors)->withInput();
        }

        // Utilizar el CHECK: coherencia de los datos según el tipo de entidad
        if (!$this->validarCoherenciaPromotor($promotorData)) {
            $error = 'Los campos proporcionados no son coherentes con el tipo de entidad seleccionado.';
            return redirect()->back()->with('errors', $error)->withInput();
        }

        // Actualizar el promotor si la validación pasa
        if ($promotorModel->update($id, $promotorData) === false) {
            $errors = $promotorModel->errors();
            return redirect()->back()->with('errors', $errors)->withInput();
        } else {
            return redirect()->to('/promotor')->with('message', 'Promotor editado con éxito');
        }
        
    }
    // función privada para dentro de actualizarEdicionPromotor
    private function validarCoherenciaPromotor($promotorData)
    {
        // Obtener el tipo de entidad del formulario
        $tipoEntidad = $promotorData['Tipo_Entidad'];

        // Validar la coherencia de los datos según el tipo de entidad
        if ($tipoEntidad == 'Fisica') {
            if (empty($promotorData['Nombre']) || empty($promotorData['Apellido1'])) {
                return false;
            }
        } elseif ($tipoEntidad == 'Juridica') {
            if (empty($promotorData['Razon_Social']) || empty($promotorData['CIF'])) {
                return false;
            }
        }

        return true;
    }
    
    /************************
     * BORRADO DE PROMOTORES
     ***********************/    
    public function borrar($id) {
        $promotorModel = new PromotorModel();
        $promotorModel->delete($id);

        return redirect()->to('/promotor')->with('message', 'Promotor eliminado con éxito');
    }
    
    /***************
     * Buscar por CIF en las solicitudes
     */
    public function buscar()
    {
        if ($this->request->isAJAX()) {
            $cif = $this->request->getVar('cif');
            $promotorModel = new PromotorModel();
            $promotor = $promotorModel->where('CIF', $cif)->first();

            if ($promotor) {
                return $this->response->setJSON([
                    'status' => 'success',
                    'data' => $promotor
                ]);
            } else {
                return $this->response->setJSON([
                    'status' => 'error',
                    'message' => 'Promotor no encontrado'
                ]);
            }
        }

        return redirect()->back();
    }
}
