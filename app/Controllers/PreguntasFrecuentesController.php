<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of Faq
 *
 * @author paula
 */
class PreguntasFrecuentesController extends BaseController {
    public function preguntasfrecuentes(){
	$data['title'] = "Preguntas frecuentes";
	return view('/PreguntasFrecuentes',$data);
    }
}
