<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
//controlador de prueba
//$routes->get('/prueba', 'Prueba::inicio');

//servicio que activa las rutas necesarias del autenticador Shield
service('auth')->routes($routes);

//*****************APP routes***************/
/*Navbar*/
//bienvenidos a canon
$routes->get('/', 'Inicio::bienvenida');


// para artistas loggeados: si quieres trabajar con nosotros, rellena del todo tus datos: regístrate
$routes->get('/registro','Inicio::bienvenida');

/********ARTISTAS*************/
//tabla artistas
$routes->get('/artistas', 'ArtistaController::index');

///////Registro - registrar artistas con el formulario de alta en la BD
//formulario de alta de nuevo artista en la BD
$routes->get('/registrar', 'ArtistaController::mostrarFormularioRegistro');
//se envían datos y manejan errores
$routes->post('/artista/procesarFormularioRegistro', 'ArtistaController::procesarFormularioRegistro');
//si hay éxito, muestra detalle de lo registrado y permite redirigir al usuario por la web
$routes->get('/artista/detalle/(:num)', 'ArtistaController::detalle/$1');

//edición artistas con el formulario de edición - 1º mostrar para editar, 2º procesar edición
$routes->get('/artista/editar/(:num)', 'ArtistaController::mostrarFormularioEdicion/$1');
$routes->post('/artista/editar/(:num)', 'ArtistaController::procesarFormularioEdicion/$1');

//borrar artistas
$routes->get('artista/borrar/(:num)', 'ArtistaController::borrar/$1');

/********PROMOTORES*************/
//tabla promotores
$routes->get('/promotores', 'PromotorController::index');

//alta nuevos promotores
$routes->get('promotor/registrar', 'PromotorController::mostrarFormularioRegistro');
$routes->post('promotor/registrar', 'PromotorController::procesarFormularioRegistro');

//edición promotores con el formulario de edición - 1º editar, 2º actualizar edición
$routes->get('/promotor/editar/(:num)', 'PromotorController::editarPromotor/$1');
$routes->post('/promotor/editar/(:num)', 'PromotorController::actualizarEdicionPromotor/$1');

//borrar promotores
$routes->get('promotor/borrar/(:num)', 'PromotorController::borrar/$1');


/************SOLICITUDES**************/
/////tabla de todas las solicitudes
$routes->get('/solicitudes', 'SolicitudController::index');
/////crear nueva solicitud:
//mostrar formulario de solicitud
$routes->get('/nueva_solicitud', 'SolicitudController::create');
//AJAX necesario para buscar el CIF del promotor para insertar en la solicitud
$routes->post('buscar','SolicitudController::buscar');
//se envían datos y manejan errores
$routes->post('/nueva_solicitud/procesar', 'SolicitudController::store');
//si hay éxito, muestra detalle de la solicitud hecha y permite redirigir al usuario por la web
$routes->get('/nueva_solicitud/detalle/(:num)', 'SolicitudController::detalle/$1');

/////editar
$routes->get('solicitud/editar/(:num)', 'SolicitudController::mostrarFormularioEdicion/$1');
$routes->post('solicitud/editar/(:num)', 'SolicitudController::procesarFormularioEdicion/$1');
/////borrar
$routes->get('solicitud/borrar/(:num)', 'SolicitudController::borrar/$1');


/*****************FACTURAS****************/
//ver facturas - listado de facturas 
$routes->get('/facturas', 'FacturaController::index');

//AGRUPACIÓN DE RUTAS
$routes->group('factura', function($routes) {
    $routes->get('crear', 'FacturaController::create');
    $routes->post('store', 'FacturaController::store');
    $routes->get('detalle/(:num)', 'FacturaController::detalle/$1');
    $routes->get('editar/(:num)', 'FacturaController::mostrarFormularioEdicion/$1');
    $routes->post('procesarFormularioEdicion/(:num)', 'FacturaController::procesarFormularioEdicion/$1');
    $routes->get('borrar/(:num)', 'FacturaController::borrar/$1');
    $routes->post('buscarSolicitud', 'FacturaController::buscarSolicitud'); 
});



/***********SERVICIOS/SIDEBAR************/
//SIMULADOR
$routes->get('simulador', 'SimuladorController::simulador');
//PREGUNTAS FRECUENTES
$routes->get('preguntas_frecuentes', 'PreguntasFrecuentesController::PreguntasFrecuentes');
//CONTACTO
$routes->get('contacto', 'ContactoController::contacto');
//QUIÉNES SOMOS
$routes->get('quienes_somos','QuienesSomosController::quienessomos');

